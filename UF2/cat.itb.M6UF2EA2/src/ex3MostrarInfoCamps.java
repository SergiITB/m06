import java.sql.*;

public class ex3MostrarInfoCamps {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");

            Statement sentencia = conexion.createStatement();

            System.out.println("--------TABLA ALUMNOS--------");
            ResultSet rsAlum = sentencia
                    .executeQuery("SELECT * FROM ALUMNOS");
            mostrarInfoTaules(rsAlum);

            System.out.println("--------TABLA ASIGNATURAS--------");
            ResultSet rsAsig = sentencia
                    .executeQuery("SELECT * FROM ASIGNATURAS");
            mostrarInfoTaules(rsAsig);

            System.out.println("--------TABLA NOTAS--------");
            ResultSet rsNot = sentencia
                    .executeQuery("SELECT * FROM NOTAS");
            mostrarInfoTaules(rsNot);

            sentencia.close();
            conexion.close();

        } catch (SQLException e) {
             e.printStackTrace();
           }
    }

    private static void mostrarInfoTaules(ResultSet rs) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();

            int nColumnas = rsmd.getColumnCount();
            String nula;
            System.out.printf("Numero de columnas recuperadas: %d%n", nColumnas);
            for (int i = 1; i <= nColumnas; i++) {
                System.out.printf("Columna %d: %n ", i);
                System.out.printf("  Nombre: %s %n   Tipo: %s %n ",
                        rsmd.getColumnName(i), rsmd.getColumnTypeName(i));
                if (rsmd.isNullable(i) == 0)
                    nula = "NO";
                else
                    nula = "SI";

                System.out.printf("  Puede ser nula?: %s %n ", nula);
                try {
                    System.out.printf("  Maximo ancho de la columna: %d %n",
                            rsmd.getColumnDisplaySize(i));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }// for

            rs.close();

        }catch (SQLException e) {
                e.printStackTrace();
            }
    }
}
