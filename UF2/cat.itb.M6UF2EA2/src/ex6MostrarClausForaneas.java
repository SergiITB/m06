import java.sql.*;

public class ex6MostrarClausForaneas {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");

            DatabaseMetaData dbmd = conexion.getMetaData();// Creamos
            // objeto DatabaseMetaData
            ResultSet resul = null;

            String[] tableNames = {"ALUMNOS", "ASIGNATURAS", "NOTAS"};

            for (String tableName : tableNames) {
                System.out.println("CLAVES ajenas que referencian a "+tableName+": ");
                System.out.println("==============================================");

                ResultSet fk = dbmd.getExportedKeys(null, "school", tableName);

                while (fk.next()) {
                    String fk_name = fk.getString("FKCOLUMN_NAME");
                    String pk_name = fk.getString("PKCOLUMN_NAME");
                    String pk_tablename = fk.getString("PKTABLE_NAME");
                    String fk_tablename = fk.getString("FKTABLE_NAME");
                    System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                    System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
                }
            }
            conexion.close(); // Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }// fin de main
}
