import java.sql.*;

public class ex4MostrarInfoCamps {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos objeto DatabaseMetaData
            ResultSet resul = null;

            String[] tableNames = {"ALUMNOS","ASIGNATURAS","NOTAS"};

            for (String tableName:tableNames) {
                System.out.println("COLUMNAS TABLA "+tableName+": ");
                System.out.println("===================================");
                ResultSet columnas = dbmd.getColumns(null, "school", tableName, null);
                while (columnas.next()) {
                    String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                    String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                    String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                    String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                    System.out.printf("Columna: %s, Tipo: %s, Tama�o: %s, �Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
                }
            }
            conexion.close(); //Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//fin de main
}
