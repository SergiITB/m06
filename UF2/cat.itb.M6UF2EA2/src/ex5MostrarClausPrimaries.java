import java.sql.*;

public class ex5MostrarClausPrimaries {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String[] tableNames = {"ALUMNOS", "ASIGNATURAS", "NOTAS"};

            for (String tableName : tableNames) {
                System.out.println("CLAVE PRIMARIA TABLA "+tableName+": ");
                System.out.println("===================================");
                ResultSet pk = dbmd.getPrimaryKeys(null, "school", tableName);
                String pkDep = "", separador = "";
                while (pk.next()) {
                    pkDep = pkDep + separador +
                            pk.getString("COLUMN_NAME");//getString(4)
                    separador = "+";
                }
                System.out.println("Clave Primaria: " + pkDep);
            }
            conexion.close(); //Cerrar conexion
        } catch (SQLException e) {e.printStackTrace();}
    }//fin de main
}
