package MetodesCRUD;

import Model.EstadisticasEntity;
import Model.JugadoresEntity;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class Estadisticas_CRUD {
    public static void insertEstadisticas(Session session, String temporada,int jugadorID, double puntos, double assis, double tapones, double rebotes, int id) {
        final Transaction tx = session.beginTransaction();

        EstadisticasEntity estadistica = new EstadisticasEntity();//Creem un objecte estadistica
        estadistica.setTemporada(temporada);
        estadistica.setJugadoresByJugador(getJugadorEntity(session,jugadorID));
        estadistica.setPuntosPorPartido(puntos);
        estadistica.setAsistenciasPorPartido(assis);
        estadistica.setTaponesPorPartido(tapones);
        estadistica.setRebotesPorPartido(rebotes);
        estadistica.setId(id);//Afegim els seus atributs
        session.save(estadistica);
        try {
            tx.commit();//Insertem l'objecte estadistica a la base de dades
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
        }
        System.out.println("ESTADISTIQUES INSERTADES CORRECTAMENT!!");
    }

    private static JugadoresEntity getJugadorEntity(Session session, int jugadorID) {
        JugadoresEntity player = null;
        try {
            player = session.load(JugadoresEntity.class, 123);//Busquem jugador amb id
        } catch (ObjectNotFoundException o) {
            System.out.println("El jugador no existeix!!");
        }
        return player;//retorna el jugador amb id indicat
    }

    public static void consultMillorJugadorTemporada(Session session) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
        Root<EstadisticasEntity> rootEntry = cq.from(EstadisticasEntity.class);
        cq.orderBy(cb.asc(rootEntry.get("temporada")));
        cq.groupBy(rootEntry.get("temporada"));
        cq.multiselect(rootEntry.get("temporada"), cb.max(rootEntry.get("puntosPorPartido")));
        TypedQuery<Tuple> query = session.createQuery(cq);

        System.out.println("Millors puntuadors per temporada");

        for (Tuple tuple: query.getResultList()) {
            CriteriaBuilder cb2 = session.getCriteriaBuilder();
            CriteriaQuery<EstadisticasEntity> cq2 = cb2.createQuery(EstadisticasEntity.class);
            Root<EstadisticasEntity> rootEntry2 = cq2.from(EstadisticasEntity.class);
            cq2.select(rootEntry2).where(
                    cb2.equal(rootEntry2.get("puntosPorPartido"),tuple.get(1)),
                    cb2.equal(rootEntry2.get("temporada"),tuple.get(0))
            );
            TypedQuery<EstadisticasEntity> query2 = session.createQuery(cq2);
            JugadoresEntity fdfd = query2.getSingleResult().getJugadoresByJugador();
            System.out.println("==================================\n"+
                    "Temporada: "+tuple.get(0)+
                    "\n"+fdfd.getCodigo()+","+fdfd.getNombre()+","+fdfd.getEquiposByNombreEquipo().getNombre()+
                    "\nPunts: "+tuple.get(1));
        }
    }
}
