package MetodesCRUD;

import Model.EquiposEntity;
import org.hibernate.Session;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Equipo_CRUD {
    public static List<EquiposEntity> consultEquips(Session session) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EquiposEntity> cq = cb.createQuery(EquiposEntity.class);
        Root<EquiposEntity> rootEntry = cq.from(EquiposEntity.class);
        CriteriaQuery<EquiposEntity> all = cq.select(rootEntry);
        TypedQuery<EquiposEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();//Retorna una llista amb tots els equips
    }
}
