package MetodesCRUD;

import Model.JugadoresEntity;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

public class Jugador_CRUD {
    public static void updatePesJugadors(Session session, int codigo, Integer newPeso){
        Transaction tx = session.beginTransaction();
        try {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaUpdate<JugadoresEntity> criteriaUpdate = cb.createCriteriaUpdate(JugadoresEntity.class);
            Root<JugadoresEntity> root = criteriaUpdate.from(JugadoresEntity.class);
            criteriaUpdate.set("peso", newPeso);
            criteriaUpdate.where(cb.equal(root.get("codigo"),codigo));//Preparem update

            session.createQuery(criteriaUpdate).executeUpdate();//Executem update
            tx.commit();

        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTEIX EL JUGADOR...");
        } catch (Exception e) {
            System.out.println("ERROR NO CONTROLAT....");
            e.printStackTrace();
        }finally {
            System.out.println("PES ACTUALITZAT !!");
        }
    }

    public static JugadoresEntity consultJugadorByCodi(Session session,int codigo) {
        TypedQuery<JugadoresEntity>query = null;
        try {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<JugadoresEntity> cq = cb.createQuery(JugadoresEntity.class);
            Root<JugadoresEntity> rootEntry = cq.from(JugadoresEntity.class);
            cq.select(rootEntry).where(cb.equal(rootEntry.get("codigo"), codigo)); //preparem consulta
            query = session.createQuery(cq);//executem consulta
        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTEIX EL JUGADOR...");
        }
        assert query != null;
        return query.getSingleResult();//Retorna un objecte jugador amb el filtre indicat
    }
}
