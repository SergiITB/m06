package Conexions;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class NbaConnection {

    //ATTRIBUTE
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    //METHODS
    /**
     * This method is used to obtain a session to the empresa database.
     * @return session
     */
    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    /**
     * This method is used to close the session to the empresa database.
     */
    public static void closeSession() {
        ourSessionFactory.close();
    }
}
