import Conexions.NbaConnection;
import MetodesCRUD.Equipo_CRUD;
import MetodesCRUD.Estadisticas_CRUD;
import MetodesCRUD.Jugador_CRUD;
import Model.EquiposEntity;
import Model.EstadisticasEntity;
import Model.JugadoresEntity;
import org.hibernate.*;
import org.hibernate.query.Query;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(final String[] args) throws Exception {
        Session session = NbaConnection.getSession();

        Scanner scanner = new Scanner(System.in);

        System.out.print("--------------MENU-----------\n" +
                "1) Exercici 1\n" +
                "2) Exercici 2\n" +
                "3) Exercici 3\n" +
                "4) Exercici 4\n" +
                "5) Exercici 5\n" +
                "Introdueix l'exercici a realitzar: ");
        int option = scanner.nextInt();
        switch (option){
            case 1: ex1(session);break;
            case 2: ex2(session);break;
            case 3: ex3(session);break;
            case 4: ex4(session);break;
            case 5: ex5(session);break;
        }
        session.close();
    }

    private static void ex5(Session session) {
       Estadisticas_CRUD.consultMillorJugadorTemporada(session);
    }

    private static void ex4(Session session) {
        List<EquiposEntity> equipos =  Equipo_CRUD.consultEquips(session);
        printEquiposInfo(equipos);
    }

    private static void ex3(Session session) {
        JugadoresEntity jugador1 = Jugador_CRUD.consultJugadorByCodi(session,227);
        printJugadorInfo(jugador1);
        JugadoresEntity jugador2 = Jugador_CRUD.consultJugadorByCodi(session,43);
        printJugadorInfo(jugador2);
        JugadoresEntity jugador3 = Jugador_CRUD.consultJugadorByCodi(session,469);
        printJugadorInfo(jugador3);
    }

    private static void ex2(Session session) {
        int[] codis = {101,251,353,561,407};
        Integer[] pesos = {240,200,300,290,263};
        for (int i = 0; i< codis.length;++i) {
            Jugador_CRUD.updatePesJugadors(session,codis[i],pesos[i]);
        }
    }

    private static void ex1(Session session) {
        Estadisticas_CRUD.insertEstadisticas(session, "05/06",123, 7.0,0.0,0.0,5.0,obtenirMaxID()+1);
        Estadisticas_CRUD.insertEstadisticas(session, "06/07",123, 10.0,0.0,0.0,3.0,obtenirMaxID()+1);
    }

    private static void printEquiposInfo(List<EquiposEntity> equipos) {
        System.out.println("Número d'Equips: "+equipos.size()+
                "\n===================================================");
        for (EquiposEntity equipo :equipos) {
            System.out.print("Equip: "+equipo.getNombre());
            for (JugadoresEntity jugador: equipo.getJugadoresByNombre()){
                Double punts = 0.0;
                Double contador = 0.0;
                for (EstadisticasEntity estadistica: jugador.getEstadisticasByCodigo()){
                    punts+=estadistica.getPuntosPorPartido();
                    contador++;
                }
                double mitjanaPunts = punts/contador;
                System.out.printf("\n%d,%s: %.2f",jugador.getCodigo(),jugador.getNombre(),mitjanaPunts);
            }
            System.out.println("\n=================================================== ");
        }
    }

    private static void printJugadorInfo(JugadoresEntity jugador) {
        System.out.println("DADES DEL JUGADOR: "+jugador.getCodigo()+
                "\nNom: "+jugador.getNombre()+
                "\nEquip: "+jugador.getEquiposByNombreEquipo().getNombre()+
                "\nTemporada  Punts  Assistències  Taps  Rebots"+
                "\n===================================================");
        for (EstadisticasEntity stats :jugador.getEstadisticasByCodigo()) {
            System.out.println("  "+stats.getTemporada()+"    "+stats.getPuntosPorPartido()+"       "+stats.getAsistenciasPorPartido()+"        "+stats.getTaponesPorPartido()+"    "+stats.getRebotesPorPartido());
        }
        System.out.println("\n=================================================== "+
                "\nNum de registres: "+ (long) jugador.getEstadisticasByCodigo().size()+
                "\n=================================================== ");
    }

    public static int obtenirMaxID() {
        Session session = NbaConnection.getSession();
        Query q = session.createQuery("select max(e.id) from EstadisticasEntity as e");
        Object res = q.uniqueResult();
        int max = (int)res;
        session.close();
        return max;
    }
}