import java.sql.*;

public class ex4 {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");
            String query = "SELECT * FROM NOTAS WHERE DNI like ?";
            PreparedStatement pst = conexion.prepareStatement(query);
            pst.setString(1, "4448242");

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                System.out.printf("DNI: %d, COD: %d, NOTA: %d %n",
                        rs.getInt(1), rs.getInt(2), rs.getInt(3));
            }
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
        }
    }
}
