import java.sql.*;

public class ex2MostrarAlumnos {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");
            String sql = "SELECT * FROM ALUMNOS";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("DNI: %d, NOM: %s, DIRECCIÓ: %s, POBLACIÓ: %s, TELEFON: %d %n",
                            rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close();
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }
}
