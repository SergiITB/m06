import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ex6 {
    public static void main(String[] args) {
        try {
            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");
            String sql = "INSERT INTO NOTAS VALUES " + "( ?, ?, ? )";
            PreparedStatement sentencia1 = conexion.prepareStatement(sql);

            String dni = "12325311"; // dni
            String cod = "4"; // cod
            String nota = "8"; // nota
            sentencia1.setInt(1, Integer.parseInt(dni));
            sentencia1.setInt(2, Integer.parseInt(cod));
            sentencia1.setInt(3, Integer.parseInt(nota));
            System.out.println(sql);

            PreparedStatement sentencia2 = conexion.prepareStatement(sql);
            dni = "12325311"; // dni
            cod = "5"; // cod
            nota = "8"; // nota
            sentencia2.setInt(1, Integer.parseInt(dni));
            sentencia2.setInt(2, Integer.parseInt(cod));
            sentencia2.setInt(3, Integer.parseInt(nota));
            System.out.println(sql);

            PreparedStatement sentencia3 = conexion.prepareStatement(sql);
            dni = "43525711"; // dni
            cod = "4"; // cod
            nota = "8"; // nota
            sentencia3.setInt(1, Integer.parseInt(dni));
            sentencia3.setInt(2, Integer.parseInt(cod));
            sentencia3.setInt(3, Integer.parseInt(nota));
            System.out.println(sql);

            PreparedStatement sentencia4 = conexion.prepareStatement(sql);
            dni = "43525711"; // dni
            cod = "5"; // cod
            nota = "8"; // nota
            sentencia4.setInt(1, Integer.parseInt(dni));
            sentencia4.setInt(2, Integer.parseInt(cod));
            sentencia4.setInt(3, Integer.parseInt(nota));
            System.out.println(sql);

            PreparedStatement sentencia5 = conexion.prepareStatement(sql);
            dni = "75827231"; // dni
            cod = "4"; // cod
            nota = "8"; // nota
            sentencia5.setInt(1, Integer.parseInt(dni));
            sentencia5.setInt(2, Integer.parseInt(cod));
            sentencia5.setInt(3, Integer.parseInt(nota));
            System.out.println(sql);

            PreparedStatement sentencia6 = conexion.prepareStatement(sql);
            dni = "75827231"; // dni
            cod = "5"; // cod
            nota = "8"; // nota
            sentencia6.setInt(1, Integer.parseInt(dni));
            sentencia6.setInt(2, Integer.parseInt(cod));
            sentencia6.setInt(3, Integer.parseInt(nota));
            System.out.println(sql);

            int filas;//
            try {
                filas = sentencia1.executeUpdate();
                filas += sentencia2.executeUpdate();
                filas += sentencia3.executeUpdate();
                filas += sentencia4.executeUpdate();
                filas += sentencia5.executeUpdate();
                filas += sentencia6.executeUpdate();
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    "+ e.getMessage());
                System.out.println("SQL estado: "+ e.getSQLState());
                System.out.println("C�d error:  "+ e.getErrorCode());
            }



            sentencia1.close(); // Cerrar Statement
            sentencia2.close(); // Cerrar Statement
            sentencia3.close(); // Cerrar Statement
            sentencia4.close(); // Cerrar Statement
            sentencia5.close(); // Cerrar Statement
            sentencia6.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
