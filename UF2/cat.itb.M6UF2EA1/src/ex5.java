import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ex5 {
    public static void main(String[] args) {
        try{
            Connection conn = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");
            Statement stmt = conn.createStatement();
            String insert = "INSERT INTO ALUMNOS (dni,apenom,direc,pobla,telef) VALUES ('12325311','Colomer Vila, Roc', 'C/ Estopenya, 24','Barcelona','935578899'),('43525711','Sanchez Delgado, David', 'C/ Sant Geroni, 22','Asturies','936739365'),('75827231','Martinez Suarez, Rey', 'C/ Catalunya, 56','Sevilla','945729661')";
            stmt.executeUpdate(insert);
            stmt.close();
            conn.close();
        }catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception ex){
            System.out.println("Exception: " + ex.getMessage());
        }
    }
}
