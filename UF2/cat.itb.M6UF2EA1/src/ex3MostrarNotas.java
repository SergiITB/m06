import java.sql.*;

public class ex3MostrarNotas {
    public static void main(String[] args) {
        try {
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");
            String sql = "SELECT * FROM NOTAS";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("DNI: %d, COD: %d, NOTA: %d %n",
                            rs.getInt(1), rs.getInt(2), rs.getInt(3));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close();
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }
}
