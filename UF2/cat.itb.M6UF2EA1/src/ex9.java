import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ex9 {
    public static void main(String[] args) {
        String poblacio = "Mostoles";

        try {
            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");

            String sql = String.format("DELETE FROM alumnos WHERE pobla = '%s'", poblacio);

            System.out.println(sql);

            Statement sentencia = conexion.createStatement();
            int filas = sentencia.executeUpdate(sql);
            System.out.printf("Alumnos eliminados: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            if (e.getErrorCode() == 1062)
                System.out.println("CLAVE PRIMARIA DUPLICADA");
            else
            if (e.getErrorCode() == 1452)
                System.out.println("CLAVE AJENA "+ poblacio + " NO EXISTE");

            else {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("C�d error:  " + e.getErrorCode());
            }
        }
    }
}
