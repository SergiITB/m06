package Model;

import java.math.BigDecimal;
import java.util.Objects;

public class DetallEntity {
    private int detallNum;
    private BigDecimal preuVenda;
    private Integer quantitat;
    private BigDecimal importe;
    private ComandaEntity comandaByComNum;
    private ProducteEntity producteByProdNum;

    public int getDetallNum() {
        return detallNum;
    }

    public void setDetallNum(int detallNum) {
        this.detallNum = detallNum;
    }

    public BigDecimal getPreuVenda() {
        return preuVenda;
    }

    public void setPreuVenda(BigDecimal preuVenda) {
        this.preuVenda = preuVenda;
    }

    public Integer getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(Integer quantitat) {
        this.quantitat = quantitat;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetallEntity that = (DetallEntity) o;
        return detallNum == that.detallNum && Objects.equals(preuVenda, that.preuVenda) && Objects.equals(quantitat, that.quantitat) && Objects.equals(importe, that.importe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(detallNum, preuVenda, quantitat, importe);
    }

    public ComandaEntity getComandaByComNum() {
        return comandaByComNum;
    }

    public void setComandaByComNum(ComandaEntity comandaByComNum) {
        this.comandaByComNum = comandaByComNum;
    }

    public ProducteEntity getProducteByProdNum() {
        return producteByProdNum;
    }

    public void setProducteByProdNum(ProducteEntity producteByProdNum) {
        this.producteByProdNum = producteByProdNum;
    }

    @Override
    public String toString() {
        return "DetallEntity{" +
                "detallNum=" + detallNum +
                ", preuVenda=" + preuVenda +
                ", quantitat=" + quantitat +
                ", importe=" + importe +
                '}';
    }
}
