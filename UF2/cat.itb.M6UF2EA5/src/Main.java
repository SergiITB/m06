import Conexions.EmpresaConnection;
import Crud.Client_CRUD;
import Crud.Comanda_CRUD;
import Crud.Empleat_CRUD;
import Model.ClientEntity;
import Model.ComandaEntity;
import Model.EmpEntity;
import org.hibernate.Session;


import javax.persistence.Tuple;
import java.math.BigInteger;
import java.sql.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(final String[] args) {
        final Session session = EmpresaConnection.getSession();
        Scanner scanner = new Scanner(System.in);

        System.out.print("--------------MENU-----------\n" +
                            "1) Exercici 1         5) Exercici 5\n" +
                            "2) Exercici 2         6) Exercici 6\n" +
                            "3) Exercici 3         7) Exercici 7\n" +
                            "4) Exercici 4\n" +
                "Introdueix l'exercici a realitzar: ");
        int option = scanner.nextInt();
        switch (option){
            case 1: ex1(session);break;
            case 2: ex2(session);break;
            case 3: ex3(session);break;
            case 4: ex4(session);break;
            case 5: ex5(session);break;
            case 6: ex6(session);break;
            case 7: ex7(session);break;
        }
    }

    private static void ex7(Session session) {
        try{
           List<Tuple> departaments = Empleat_CRUD.consultEmpNumOfDep(session);
            for (Tuple departament : departaments){
                BigInteger deptNo = (BigInteger) departament.get(0);
                Long count = (Long) departament.get(1);
                System.out.println("Num departament:"+deptNo+" Empleats:"+count);
            }
        } finally {
            session.close();
        }
    }

    private static void ex6(Session session) {
        try{
            List<EmpEntity> empleats = Empleat_CRUD.consultEmpBy_salaryMin(session, BigInteger.valueOf(300000));
            for (EmpEntity empleat:empleats){
                System.out.println("Cognom:"+empleat.getCognom()+" Ofici:"+empleat.getOfici());
            }
        } finally {
            session.close();
        }
    }

    private static void ex5(Session session) {
        try{
            List<ComandaEntity> comandes = Comanda_CRUD.consultComandaBy_date(session,Date.valueOf("1986-01-01"),Date.valueOf("1986-09-30"));
            for (ComandaEntity comanda:comandes) {
                System.out.println(comanda.getComNum());
            }
        } finally {
            session.close();
        }
    }

    private static void ex4(Session session) {
        try{
            EmpEntity empleat = Empleat_CRUD.consultEmpBy_cognom(session,"CEREZO");
            System.out.println(empleat);
            session.close();
        } finally {
            session.close();
        }
    }

    private static void ex3(Session session) {
        try{
            List<ClientEntity> clients = Client_CRUD.consultClientBy_ciutat(session,"BURLINGAME");
            for (ClientEntity client : clients){
                System.out.println("Nom:"+client.getNom()+" Telefon:"+client.getTelefon());
            }
        } finally {
            session.close();
        }
    }

    private static void ex2(Session session) {
        try{
            List<ComandaEntity> coms = Comanda_CRUD.consultComandaBy_comTipusIsNull(session);
            for (ComandaEntity com : coms) {
                System.out.println("Nom: "+com.getClientByClientCod().getNom()+" ComData:"+com.getComData());
            }
        } finally {
            session.close();
        }
    }

    private static void ex1(Session session) {
        try {
            List<ComandaEntity> coms = Comanda_CRUD.consultAllComandas(session);
            for (ComandaEntity com : coms) {
                System.out.println(com);
            }
        } finally {
            session.close();
        }
    }
}