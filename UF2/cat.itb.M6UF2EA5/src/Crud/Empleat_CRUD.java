package Crud;

import Model.EmpEntity;
import org.hibernate.Session;


import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.List;

public class Empleat_CRUD {
    public static EmpEntity consultEmpBy_cognom(Session session, String cognom) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EmpEntity> cq = cb.createQuery(EmpEntity.class);
        Root<EmpEntity> rootEntry = cq.from(EmpEntity.class);
        cq.select(rootEntry).where(
                cb.equal(rootEntry.get("cognom"), cognom)
        );
        TypedQuery<EmpEntity> query = session.createQuery(cq);

        return query.getSingleResult();
    }

    public static List<EmpEntity> consultEmpBy_salaryMin(Session session, BigInteger salary_min) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EmpEntity> cq = cb.createQuery(EmpEntity.class);
        Root<EmpEntity> rootEntry = cq.from(EmpEntity.class);
        cq.select(rootEntry).where(
                cb.greaterThan(rootEntry.get("salari"), salary_min)
        );
        TypedQuery<EmpEntity> query = session.createQuery(cq);

        return query.getResultList();
    }

    public static List<Tuple> consultEmpNumOfDep(Session session){
        CriteriaBuilder cb4 = session.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq4 = cb4.createQuery(Tuple.class);
        Root<EmpEntity> rootEntry4 = cq4.from(EmpEntity.class);
        cq4.groupBy(rootEntry4.get("deptByDeptNo").get("deptNo"));

        cq4.multiselect(rootEntry4.get("deptByDeptNo").get("deptNo"),
                cb4.count(rootEntry4.get("empNo")));

        TypedQuery<Tuple> allQuery4 = session.createQuery(cq4);
        return allQuery4.getResultList();
    }
}