package Crud;

import Model.ClientEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Client_CRUD {
    public static List<ClientEntity> consultClientBy_ciutat(Session session, String ciutat) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> cq = cb.createQuery(ClientEntity.class);
        Root<ClientEntity> rootEntry = cq.from(ClientEntity.class);
        cq.select(rootEntry).where(
                cb.equal(rootEntry.get("ciutat"), ciutat)
        );
        TypedQuery<ClientEntity> query = session.createQuery(cq);

        return query.getResultList();
    }
}
