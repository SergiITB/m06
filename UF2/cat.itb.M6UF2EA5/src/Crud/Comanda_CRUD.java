package Crud;

import Model.ComandaEntity;
import org.hibernate.Session;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.util.List;

public class Comanda_CRUD {
    public static List<ComandaEntity> consultAllComandas(Session session) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cq = cb.createQuery(ComandaEntity.class);
        Root<ComandaEntity> rootEntry = cq.from(ComandaEntity.class);
        CriteriaQuery<ComandaEntity> all = cq.select(rootEntry);
        TypedQuery<ComandaEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    public static List<ComandaEntity> consultComandaBy_comTipusIsNull(Session session) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cq = cb.createQuery(ComandaEntity.class);
        Root<ComandaEntity> rootEntry = cq.from(ComandaEntity.class);
        cq.select(rootEntry).where(cb.isNull(rootEntry.get("comTipus")));
        TypedQuery<ComandaEntity> query = session.createQuery(cq);
        return query.getResultList();
    }

    public static List<ComandaEntity> consultComandaBy_date(Session session, Date dataMin, Date dataMax) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cq = cb.createQuery(ComandaEntity.class);
        Root<ComandaEntity> rootEntry = cq.from(ComandaEntity.class);
        cq.select(rootEntry).where(
                cb.between(rootEntry.get("dataTramesa"),dataMin,dataMax));
        TypedQuery<ComandaEntity> query = session.createQuery(cq);
        return query.getResultList();
    }
}
