package ConnexionsBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HrConnection {
    //ATTRIBUTE
    private static Connection connection;

    //METHODS
    /**
     * This method is used to obtain a connection to the hr database.
     * @return connection
     */
    public static Connection getConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/hr", "hr", "hr");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * This method is used to close the connection to the hr database.
     */
    public static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
