import ConnexionsBD.HrConnection;

import java.io.*;
import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.print("      ---------MENU--------" +
                "\n1) Exercici 1       6) Exercici 6        11) Exercici 11       16) Exercici 16" +
                "\n2) Exercici 2       7) Exercici 7        12) Exercici 12" +
                "\n3) Exercici 3       8) Exercici 8        13) Exercici 13" +
                "\n4) Exercici 4       9) Exercici 9        14) Exercici 14" +
                "\n5) Exercici 5       10) Exercici 10      15) Exercici 15" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                ejecutarScriptMySQL();
                break;
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
            case 4:
                ex4();
                break;
            case 5:
                ex5();
                break;
            case 6:
                ex6();
                break;
            case 7:
                ex7();
                break;
            case 8:
                ex8();
                break;
            case 9:
                ex9();
                break;
            case 10:
                ex10();
                break;
            case 11:
                ex11();
                break;
            case 12:
                ex12();
                break;
            case 13:
                ex13();
                break;
            case 14:
                ex14();
                break;
            case 15:
                ex15();
                break;
            case 16:
                ex16();
                break;
        }
    }

    private static void ex16() {
        try {
            Connection conexion = HrConnection.getConnection();

            Scanner scanner = new Scanner(System.in);
            System.out.print("Introdueix el numeró de departament: ");
            String dep = scanner.next();   // departamento

            String sql = "{ ? = call nombre_dep (?) } "; // MYSQL

            // Preparamos la llamada
            CallableStatement llamada = conexion.prepareCall(sql);

            llamada.registerOutParameter(1, Types.VARCHAR); // valor devuelto
            llamada.setInt(2, Integer.parseInt(dep)); // param de entrada

           //llamada.executeUpdate(); // ejecutar el procedimiento
            if (llamada.executeUpdate() == 0){
                System.out.println("Nombre Dep: " + llamada.getString(1));
            }

            llamada.close();
            conexion.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void ex15() {
        try {
            Connection conexion = HrConnection.getConnection();

            Scanner scanner = new Scanner(System.in);
            System.out.print("Introdueix el numeró de departament: ");
            String dep = scanner.next();   // departamento
            System.out.print("Introdueix la quantitat a pujar el salari dels treballadors: ");
            String subida = scanner.next();// subida

            // construir orden DE LLAMADA
            String sql = " CALL subida_sal (?, ?) ";

            // Preparamos la llamada
            CallableStatement llamada = conexion.prepareCall(sql);
            // Damos valor a los argumentos
            llamada.setInt(1, Integer.parseInt(dep)); // primer argumento-dep
            llamada.setInt(2, Integer.parseInt(subida)); // segundo arg

            llamada.executeUpdate(); // ejecutar el procedimiento
            System.out.println("Subida realizada....");
            llamada.close();
            conexion.close();
        } catch (SQLException cn) {
            cn.printStackTrace();
        }
    }

    private static void ex14() {
        mostrarProcedimientosYfunciones();
    }

    private static void ex13() {
        crearFuncio();
    }

    private static void ex12() {
        crearProcediment();
    }



    private static void ex11() {
        try {
            Connection conexion = HrConnection.getConnection();

            crearVista();

            String sql="SELECT * FROM totales";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if(valor){
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("numDep: %s, nombreDep: %s, numEmpleados: %d, mediaSalario: %d %n",
                            rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void ex10() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Intrdueix l'identificador de l'empleat: ");
        String emp_no = scanner.next();
        System.out.print("Introdueix la quantitat a augmentar el salari: ");
        String quantitat = scanner.next();
        updateSalari(emp_no,quantitat);
    }



    private static void ex9() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("INTRODUEIX LES DADES DELS EMPLEATS A INSERTAR:");
        for (int i = 1; i<4; ++i){
            System.out.println("EMPLEAT "+i+": ");
            System.out.print("Introdueix num de empleat: ");
            int emp_no = scanner.nextInt(); // num. empleat
            System.out.print("Introdueix el cognom: ");
            String apellido = scanner.next(); // apellido
            scanner.nextLine();
            System.out.print("Introdueix l'ofici: ");
            String oficio = scanner.next(); // oficio
            scanner.nextLine();
            System.out.print("Introdueix dir: ");
            int dir = scanner.nextInt(); // dir
            System.out.print("Introdueix la data d'alta: ");
            String fecha_alt = scanner.next(); // fecha_alt
            System.out.print("Introdueix salari: ");
            double salario = scanner.nextDouble(); // salario
            System.out.print("Introdueix comisió: ");
            double comision = scanner.nextDouble(); // comision
            System.out.print("Introdueix numero de departament: ");
            int dept_no = scanner.nextInt(); // dept_no
            insertarEmpleat(emp_no,apellido,oficio,dir,fecha_alt,salario,comision,dept_no);
        }
    }



    private static void ex8() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("INTRODUEIX LES DADES DELS DEPARTAMENTS A INSERTAR:");
        for (int i = 1; i<4; ++i){
            System.out.println("DEPARTAMENT "+i+": ");
            System.out.print("Introdueix num de departament: ");
            int dept_no = scanner.nextInt(); // num. departamento
            System.out.print("Introdueix nom del departament: ");
            String dnombre = scanner.next(); // nombre
            scanner.nextLine();
            System.out.print("Introdueix localitat del departament: ");
            String loc = scanner.next(); // localidad
            insertarDepartament(dept_no,dnombre,loc);
        }
    }

    private static void ex7() {
        try{
            Connection conexion = HrConnection.getConnection();
            String sql="SELECT * FROM departamentos";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);
            System.out.println("DADES TAULA DEPARTAMENTOS:");

            if(valor){
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("%d, %s, %s %n",
                            rs.getInt(1), rs.getString(2), rs.getString(3));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close();
            conexion.close(); } catch (SQLException e) {e.printStackTrace();}
    }

    private static void ex6() {
        try{
            Connection conexion = HrConnection.getConnection();
            String sql="SELECT * FROM empleados";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);
            System.out.println("DADES TAULA EMPLEADOS:");

            if(valor){
                ResultSet rs = sentencia.getResultSet();
                int i = 1;
                while (rs.next())
                    System.out.printf("%d, %s, %s, %d, %s, %d, %d, %d %n",
                            rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

        sentencia.close();
        conexion.close();
        } catch (SQLException e) { e.printStackTrace();}
    }

    private static void ex5() {
        String table = "departamentos";
        getPrimaryKeys(table);
        getExportedKeys(table);

        String table2 = "empleados";
        getPrimaryKeys(table2);
        getExportedKeys(table2);
    }

    private static void ex4() {
        String table = "departamentos";
        mostarInfoCampsTaula(table);
    }

    private static void ex3() {
        String table = "empleados";
        mostarInfoCampsTaula(table);
    }

    private static void ex2() {
        mostrarMetadatesBdHR();
    }

    private static void mostrarProcedimientosYfunciones() {
        try {
            Connection conexion = HrConnection.getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();// Creamos objeto DatabaseMetaData

            ResultSet proc = dbmd.getProcedures(null, "public", null);
            ResultSet func = dbmd.getFunctions(null, "public", null);
            while (proc.next()) {
                String proc_name = proc.getString("PROCEDURE_NAME");
                String proc_type = proc.getString("PROCEDURE_TYPE");
                System.out.printf("Nombre Procedimiento: %s - Tipo: %s %n", proc_name, proc_type);
            }
            while (func.next()){
                String func_name = func.getString("FUNCTION_NAME");
                String func_type = func.getString("FUNCTION_TYPE");
                System.out.printf("Nombre Funcion: %s - Tipo: %s %n", func_name, func_type);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void crearFuncio() {
        try {
            Connection conexion = HrConnection.getConnection();
            String sql = "CREATE OR REPLACE FUNCTION nombre_dep (d int) RETURNS VARCHAR(15) as $$ DECLARE nom VARCHAR(15); BEGIN SELECT dnombre INTO nom FROM departamentos WHERE dept_no=d; IF nom is null THEN RETURN 'INEXISTENT'; ELSE RETURN nom; END IF; END; $$  language plpgsql;";

            Statement sentencia = conexion.createStatement();
            int filas = sentencia.executeUpdate(sql);

            System.out.printf("Resultat de l'execuci�: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void crearProcediment() {
        try {
            Connection conexion = HrConnection.getConnection();
            StringBuilder sql = new StringBuilder();
            sql.append("CREATE PROCEDURE subida_sal (d INT, subida INT) as $$");
            sql.append("BEGIN ");
            sql.append("UPDATE empleados SET salario = salario + subida WHERE dept_no = d; ");
            sql.append("COMMIT; " );
            sql.append("END; $$  language plpgsql;");

            System.out.println(sql);

            Statement sentencia = conexion.createStatement();
            int filas = sentencia.executeUpdate(sql.toString());
            System.out.printf("Resultado  de la ejecuci�n: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void crearVista() {
        try {
            Connection conexion = HrConnection.getConnection();
            StringBuilder sql = new StringBuilder();
            sql.append("CREATE OR REPLACE VIEW totales ");
            sql.append("(dep, dnombre, nemp, media) AS ");
            sql.append("SELECT d.dept_no, dnombre, COUNT(emp_no), AVG(salario) ");
            sql.append("FROM departamentos d LEFT JOIN empleados e " );
            sql.append("ON e.dept_no = d.dept_no ");
            sql.append("GROUP BY d.dept_no, dnombre ");
            System.out.println(sql);

            Statement sentencia = null;

            sentencia = conexion.createStatement();

            int filas = 0;

            filas = sentencia.executeUpdate(sql.toString());

            System.out.printf("Resultado  de la ejecuci�n: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void updateSalari(String emp_no, String quantitat) {
        try {
            Connection conexion = HrConnection.getConnection();

            String sql = String.format("UPDATE empleados SET salario = salario + %s WHERE emp_no = %s",
                    quantitat, emp_no);

            System.out.println(sql);

            Statement sentencia = conexion.createStatement();
            int filas = sentencia.executeUpdate(sql);
            System.out.printf("Empleados modificados: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            if (e.getErrorCode() == 1062)
                System.out.println("CLAVE PRIMARIA DUPLICADA");
            else
            if (e.getErrorCode() == 1452)
                System.out.println("CLAVE AJENA "+ emp_no + " NO EXISTE");

            else {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("C�d error:  " + e.getErrorCode());
            }
        }
    }

    private static void insertarEmpleat(int emp_no, String apellido, String oficio, int dir, String fecha_alt, double salario, double comision, int dept_no) {
        try {
            Connection conexion = HrConnection.getConnection();

            // construir orden INSERT
            String sql = "INSERT INTO empleados VALUES "
                    + "( ?, ?, ?, ?, ?, ?, ?, ? )";

            System.out.println(sql);
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, emp_no);
            sentencia.setString(2, apellido);
            sentencia.setString(3, oficio);
            sentencia.setInt(4, dir);
            sentencia.setDate(5, Date.valueOf(fecha_alt));
            sentencia.setDouble(6,salario);
            sentencia.setDouble(7, comision);
            sentencia.setInt(8,dept_no);

            int filas;
            try {
                filas = sentencia.executeUpdate();
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("C�d error:  " + e.getErrorCode());
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertarDepartament(int dept_no, String dnombre, String loc) {
        try {
            Connection conexion = HrConnection.getConnection();

            // construir orden INSERT
            String sql = "INSERT INTO departamentos VALUES "
                    + "( ?, ?, ? )";

            System.out.println(sql);
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, dept_no);
            sentencia.setString(2, dnombre);
            sentencia.setString(3, loc);

            int filas;
            try {
                filas = sentencia.executeUpdate();
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("C�d error:  " + e.getErrorCode());
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void mostarInfoCampsTaula(String table) {
        try {
            //Establecemos la conexion con la BD
            Connection conexion = HrConnection.getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("\nCOLUMNAS TABLA "+table+": ");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns(null, "public", table, null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tama駉: %s, 縋uede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
            }
            HrConnection.closeConnection(); //Cerrar conexion
        } catch (SQLException e) {e.printStackTrace();}
    }

    private static void getExportedKeys(String table) {
        try {
            Connection conexion = HrConnection.getConnection();
            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData

            System.out.println("\nCLAVES AJENAS QUE REFERENCIAN A "+table+": ");
            System.out.println("==============================================");

            ResultSet fk = dbmd.getExportedKeys(null, "public", table);

            while (fk.next()) {
                String fk_name = fk.getString("FKCOLUMN_NAME");
                String pk_name = fk.getString("PKCOLUMN_NAME");
                String pk_tablename = fk.getString("PKTABLE_NAME");
                String fk_tablename = fk.getString("FKTABLE_NAME");
                System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
            }

            conexion.close(); //Cerrar conexion
        } catch (SQLException e) {e.printStackTrace();}
    }

    private static void getPrimaryKeys(String table) {
        try {
            Connection conexion = HrConnection.getConnection();
            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData

            System.out.println("\nCLAVE PRIMARIA TABLA "+table+": ");
            System.out.println("===================================");
            ResultSet pk = dbmd.getPrimaryKeys(null, "public", table);
            String pkDep="", separador="";
            while (pk.next()) {
                pkDep = pkDep + separador +
                        pk.getString("COLUMN_NAME");//getString(4)
                separador="+";
            }
            System.out.println("Clave Primaria: " + pkDep);

            conexion.close(); //Cerrar conexion
        } catch (SQLException e) {e.printStackTrace();}
    }

    private static void mostrarMetadatesBdHR() {
        try {
            //Establecemos la conexion con la BD
            Connection conexion = HrConnection.getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String nombre  = dbmd.getDatabaseProductName();
            String driver  = dbmd.getDriverName();
            String url     = dbmd.getURL();
            String usuario = dbmd.getUserName() ;

            System.out.println("INFORMACI覰 SOBRE LA BASE DE DATOS:");
            System.out.println("===================================");
            System.out.printf("Nombre : %s %n", nombre );
            System.out.printf("Driver : %s %n", driver );
            System.out.printf("URL    : %s %n", url );
            System.out.printf("Usuario: %s %n", usuario );

            //Obtener informaci髇 de las tablas y vistas que hay
            resul = dbmd.getTables(null, "public", null, null);

            while (resul.next()) {
                String catalogo = resul.getString(1);//columna 1
                String esquema = resul.getString(2); //columna 2
                String tabla = resul.getString(3);   //columna 3
                String tipo = resul.getString(4);    //columna 4
                System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                        tipo, catalogo, esquema, tabla);
            }
            HrConnection.closeConnection(); //Cerrar conexion
        }
        catch (SQLException cn) {cn.printStackTrace();}
    }

    public static void ejecutarScriptMySQL() {
        File scriptFile = new File("cat.itb.M6UF2EA3/HR.sql");
        System.out.println("\n\nFichero de consulta : " + scriptFile.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);
        try {
            Connection connmysql = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/hr?allowMultiQueries=true","hr", "hr");
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            connmysql.close();
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }
}
