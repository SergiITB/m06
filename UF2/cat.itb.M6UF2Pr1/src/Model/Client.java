package Model;

public class Client {
    int client_cod;
    String nom;
    String adreca;
    String ciutat;
    String estat;
    String codi_postal;
    int area;
    String telefon;
    int repr_cod;
    double limit_credit;
    String observacions;

    public Client(int client_cod, String nom, String adreca, String ciutat, String estat, String codi_postal, int area, String telefon, int repr_cod, double limit_credit, String observacions) {
        this.client_cod = client_cod;
        this.nom = nom;
        this.adreca = adreca;
        this.ciutat = ciutat;
        this.estat = estat;
        this.codi_postal = codi_postal;
        this.area = area;
        this.telefon = telefon;
        this.repr_cod = repr_cod;
        this.limit_credit = limit_credit;
        this.observacions = observacions;
    }

    public int getClient_cod() {
        return client_cod;
    }

    public String getNom() {
        return nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public String getCodi_postal() {
        return codi_postal;
    }

    public int getArea() {
        return area;
    }

    public String getTelefon() {
        return telefon;
    }

    public int getRepr_cod() {
        return repr_cod;
    }

    public double getLimit_credit() {
        return limit_credit;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setClient_cod(int client_cod) {
        this.client_cod = client_cod;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public void setCodi_postal(String codi_postal) {
        this.codi_postal = codi_postal;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public void setRepr_cod(int repr_cod) {
        this.repr_cod = repr_cod;
    }

    public void setLimit_credit(int limit_credit) {
        this.limit_credit = limit_credit;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    @Override
    public String toString() {
        return "Client{" +
                "client_cod=" + client_cod +
                ", nom='" + nom + '\'' +
                ", adreca='" + adreca + '\'' +
                ", ciutat='" + ciutat + '\'' +
                ", estat='" + estat + '\'' +
                ", codi_postal='" + codi_postal + '\'' +
                ", area=" + area +
                ", telefon='" + telefon + '\'' +
                ", repr_cod=" + repr_cod +
                ", limit_credit=" + limit_credit +
                ", observacions='" + observacions + '\'' +
                '}';
    }
}
