package Model;
import java.sql.Date;

public class Empleat {
    Integer emp_no;
    String cognom;
    String ofici;
    Integer cap;
    Date data_alta;
    Integer salari;
    Integer comissio;
    Integer dept_no;

    public Empleat(Integer emp_no, String cognom, String ofici, Integer cap, Date data_alta, Integer salari, Integer comissio, Integer dept_no) {
        this.emp_no = emp_no;
        this.cognom = cognom;
        this.ofici = ofici;
        this.cap = cap;
        this.data_alta = data_alta;
        this.salari = salari;
        this.comissio = comissio;
        this.dept_no = dept_no;
    }

    public Integer getEmp_no() {
        return emp_no;
    }

    public String getCognom() {
        return cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public Integer getCap() {
        return cap;
    }

    public Date getData_alta() {
        return data_alta;
    }

    public Integer getSalari() {
        return salari;
    }

    public Integer getComissio() {
        return comissio;
    }

    public int getDept_no() {
        return dept_no;
    }

    public void setEmp_no(Integer emp_no) {
        this.emp_no = emp_no;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public void setData_alta(Date data_alta) {
        this.data_alta = data_alta;
    }

    public void setSalari(Integer salari) {
        this.salari = salari;
    }

    public void setComissio(Integer comissio) {
        this.comissio = comissio;
    }

    public void setDept_no(Integer dept_no) {
        this.dept_no = dept_no;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "emp_no=" + emp_no +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", cap=" + cap +
                ", data_alta=" + data_alta +
                ", salari=" + salari +
                ", comissio=" + comissio +
                ", dept_no=" + dept_no +
                '}';
    }
}
