package Model;



public class Producte {
    int prod_num;
    String descripcio;

    public Producte(int prod_num, String descripcio) {
        this.prod_num = prod_num;
        this.descripcio = descripcio;
    }

    public int getProd_num() {
        return prod_num;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setProd_num(int prod_num) {
        this.prod_num = prod_num;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public String toString() {
        return "Producte{" +
                "prod_num=" + prod_num +
                ", descripcio='" + descripcio + '\'' +
                '}';
    }
}
