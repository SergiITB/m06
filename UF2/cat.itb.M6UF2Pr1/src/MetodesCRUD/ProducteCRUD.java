package MetodesCRUD;

import ConnexionsBD.EmpresaConnection;
import Model.Producte;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProducteCRUD {

    public static void insertProducte(Producte producte){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "INSERT INTO PRODUCTE VALUES (?, ?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, producte.getProd_num());
            pst.setString(2, producte.getDescripcio());
            pst.executeUpdate();
            EmpresaConnection.closeConnection();
        } catch (SQLException ex) {
            System.out.println("ERROR AL INSERTAR");
            ex.printStackTrace();
        }
    }

    public static void consultProducte(int prod_num){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "SELECT * FROM PRODUCTE WHERE prod_num = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, prod_num);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Producte producte = new Producte(rs.getInt(1),rs.getString(2));
                System.out.println(producte);
            }
            EmpresaConnection.closeConnection();
            conn.close();
        }catch (SQLException ex) {
            System.out.println("ERROR AL FER LA CONSULTA");
            ex.printStackTrace();
        }
    }

    public static void deleteProducte(int prod_num){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "DELETE FROM PRODUCTE WHERE prod_num = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, prod_num);
            pst.executeUpdate();
            EmpresaConnection.closeConnection();
        }catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR PRODUCTE");
            ex.printStackTrace();
        }
    }
}
