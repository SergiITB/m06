package MetodesCRUD;

import ConnexionsBD.EmpresaConnection;
import Model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientCRUD {
    public static void updateLimitCreditClient(int client_cod, double limit_credit){
        Connection conn = EmpresaConnection.getConnection();
        try{
            String query = "UPDATE CLIENT SET limit_credit = ? WHERE client_cod = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setDouble(1, limit_credit);
            pst.setInt(2, client_cod);
            pst.executeUpdate();
            EmpresaConnection.closeConnection();
        }catch (SQLException e) {
            if (e.getErrorCode() == 1062)
                System.out.println("CLAVE PRIMARIA DUPLICADA");
            else
            if (e.getErrorCode() == 1452)
                System.out.println("CLAVE AJENA NO EXISTE");

            else {
                System.out.println("HA OCURRIDO UNA EXCEPCIÓN:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("COD error:  " + e.getErrorCode());
            }
        }
    }

    public static void consultClient(int client_cod){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "SELECT * FROM CLIENT WHERE client_cod = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, client_cod);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Client client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7),rs.getString(8),rs.getInt(9),rs.getDouble(10),rs.getString(11));
                System.out.println(client);
            }
            rs.close();
            EmpresaConnection.closeConnection();
        }catch (SQLException ex) {
            System.out.println("ERROR AL FER LA CONSULTA");
            ex.printStackTrace();
        }
    }

    public static void deleteClient(int client_cod){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "DELETE FROM CLIENT WHERE client_cod = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, client_cod);
            pst.executeUpdate();
            EmpresaConnection.closeConnection();
        }catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR EL CLIENT");
            ex.printStackTrace();
        }
    }
}
