package MetodesCRUD;

import ConnexionsBD.EmpresaConnection;
import Model.Empleat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmpleatCRUD {
    public static void insertEmpleat(Empleat empleat){
        Connection conn = EmpresaConnection.getConnection();
        try{
            String query = "INSERT INTO EMP VALUES (?, ?, ?,?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, empleat.getEmp_no());
            pst.setString(2, empleat.getCognom());
            pst.setString(3, empleat.getOfici());
            pst.setInt(4, empleat.getCap());
            pst.setDate(5, empleat.getData_alta());
            pst.setInt(6,empleat.getSalari());
            if (empleat.getComissio()==null){
                pst.setNull(7,0);
            }else {
                pst.setInt(7,empleat.getComissio());
            }
            pst.setInt(8,empleat.getDept_no());
            pst.executeUpdate();
            EmpresaConnection.closeConnection();
        }catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception ex){
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    public static void consultEmpleat(int emp_no){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "SELECT * FROM EMP WHERE emp_no = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, emp_no);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Empleat empleat = new Empleat(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getDate(5),rs.getInt(6),rs.getInt(7),rs.getInt(8));
                System.out.println(empleat);
            }
            rs.close();
            EmpresaConnection.closeConnection();
        }catch (SQLException ex) {
            System.out.println("ERROR AL FER LA CONSULTA");
            ex.printStackTrace();
        }
    }

    public static void deleteEmpleat(int emp_no){
        Connection conn = EmpresaConnection.getConnection();
        try {
            String query = "DELETE FROM EMP WHERE emp_no = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, emp_no);
            pst.executeUpdate();
            EmpresaConnection.closeConnection();
        }catch (SQLException ex) {
            System.out.println("ERROR AL ELIMINAR EMPLEAT");
            ex.printStackTrace();
        }
    }
}
