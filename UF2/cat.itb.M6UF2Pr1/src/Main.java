import MetodesCRUD.ClientCRUD;
import MetodesCRUD.EmpleatCRUD;
import MetodesCRUD.ProducteCRUD;
import Model.Empleat;
import Model.Producte;

import java.io.*;
import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("      ---------MENU--------" +
                "\n1) Exercici 1       6) Exercici 6" +
                "\n2) Exercici 2       7) Exercici 7 " +
                "\n3) Exercici 3       8) Exercici 8" +
                "\n4) Exercici 4       9) Exercici 9" +
                "\n5) Exercici 5       10) Exercici 10 " +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                ex1();
                break;
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
            case 4:
                ex4();
                break;
            case 5:
                ex5();
                break;
            case 6:
                ex6();
                break;
            case 7:
                ex7();
                break;
            case 8:
                ex8();
                break;
            case 9:
                ex9();
                break;
            case 10:
                ex10();
                break;
        }
    }

    private static void ex10() {
        ProducteCRUD.deleteProducte(400552);
    }

    private static void ex9() {
        EmpleatCRUD.deleteEmpleat(4885);
    }

    private static void ex8() {
        ClientCRUD.deleteClient(109);
    }

    private static void ex7() {
        ProducteCRUD.consultProducte(101860);
    }

    private static void ex6() {
        EmpleatCRUD.consultEmpleat(7788);
    }

    private static void ex5() {
        ClientCRUD.consultClient(106);
    }

    private static void ex4() {
        ClientCRUD.updateLimitCreditClient(104,20000);
        ClientCRUD.updateLimitCreditClient(106,12000);
        ClientCRUD.updateLimitCreditClient(107,20000);
    }

    private static void ex3() {
        Empleat empleat1 = new Empleat(4885, "BORREL", "EMPLEAT",7902,Date.valueOf("1981-12-25"),104000,null,30);
        EmpleatCRUD.insertEmpleat(empleat1);

        Empleat empleat2 = new Empleat(8772, "PUIG", "VENEDOR",7698,Date.valueOf("1990-01-23"),108000,null,30);
        EmpleatCRUD.insertEmpleat(empleat2);

        Empleat empleat3 = new Empleat(9945, "FERRER", "ANALISTA",7698,Date.valueOf("1988-05-17"),169000,39000,20);
        EmpleatCRUD.insertEmpleat(empleat3);
    }

    private static void ex2() {
        Producte producte1 = new Producte(300388,"RH GUIDE TO PADDLE");
        ProducteCRUD.insertProducte(producte1);

        Producte producte2 = new Producte(400552,"RH GUIDE TO BOX");
        ProducteCRUD.insertProducte(producte2);

        Producte producte3 = new Producte(400333,"ACE TENNIS BALLS-10 PACK");
        ProducteCRUD.insertProducte(producte3);
    }

    private static void ex1() {
        ejecutarScriptMySQL();
    }

    public static void ejecutarScriptMySQL() {
        File scriptFile = new File("cat.itb.M6UF2Pr1/src/empresa.sql");
        System.out.println("\n\nFichero de consulta : " + scriptFile.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);
        try {
            Connection connmysql = DriverManager.getConnection("jdbc:postgresql://192.168.254.4:5432/school", "school", "school");
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            connmysql.close();
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }
}
