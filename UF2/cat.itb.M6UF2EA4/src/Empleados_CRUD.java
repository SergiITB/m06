import Model.EmpleadosEntity;
import org.hibernate.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public class Empleados_CRUD {
    public static void insertEmp(SessionFactory sesionFactory, int emp_no, String apellido, String oficio, int dir, String fecha_alt, double salario, double comision, int dept_no){
        Session session = sesionFactory.openSession();
        Transaction tx = session.beginTransaction();

        EmpleadosEntity emp = new EmpleadosEntity();
        emp.setEmpNo(emp_no);
        emp.setApellido(apellido);
        emp.setOficio(oficio);
        emp.setDir(dir);
        emp.setFechaAlt(Date.valueOf(fecha_alt));
        emp.setSalario(BigDecimal.valueOf(salario));
        emp.setComision(BigDecimal.valueOf(comision));
        emp.setDepartamentosByDeptNo(emp.getDepartamentosByDeptNo());
        session.save(emp);

        tx.commit(); // valida la transaccion
        session.close();
    }

    public static void updateEmpSal(SessionFactory sessionFactory, int emp_no, double newSalari) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            EmpleadosEntity emp = (EmpleadosEntity) session.load(EmpleadosEntity.class, emp_no);
            System.out.printf("Salari antic: %s%n", emp.getSalario());
            emp.setSalario(BigDecimal.valueOf(newSalari));
            session.update(emp); // modifica el departament a la taula
            tx.commit();
            System.out.printf("Salari nou: %s%n", emp.getSalario());
        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTE EL EMPLEADO...");
        } catch (Exception e) {
            System.out.println("ERROR NO CONTROLADO....");
            e.printStackTrace();
        }

        session.close();
    }

    public static void deleteEmpByName(SessionFactory sessionFactory, String apellido) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        String hql = "from EmpleadosEntity where apellido = :apellido";
        org.hibernate.Query q = session.createQuery(hql);
        q.setParameter("apellido", apellido);
        EmpleadosEntity empleado = (EmpleadosEntity) q.uniqueResult();

        EmpleadosEntity emp = (EmpleadosEntity) session.load(EmpleadosEntity.class, empleado.getEmpNo());

        try {
            session.delete(emp); // elimina el objeto
            tx.commit();
            System.out.println("Empleado eliminado");
        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTE EL EMPLEADO...");
        } catch (Exception e) {
            System.out.println("ERROR NO CONTROLADO....");
            e.printStackTrace();
        }
        session.close();
    }

    public static void consultationWithMinSalary(SessionFactory sessionFactory, double salary) {
        Session session = sessionFactory.openSession();

        BigDecimal minSalary = BigDecimal.valueOf(salary);
        Query query = session.createQuery("from EmpleadosEntity emp where emp.salario >= :minSalary");
        query.setParameter("minSalary", minSalary);
        List<EmpleadosEntity> lista = query.list();
        System.out.println("-----------------------------------\n" +
                "EMPLEATS QUE COBRIN MÉS DE "+salary+":\n-----------------------------------");
        for (EmpleadosEntity emp : lista) {
            System.out.printf("%d, %s, %s, %d, %s, %.2f, %.2f %n",
                emp.getEmpNo(), emp.getApellido(), emp.getOficio(), emp.getDir(), emp.getFechaAlt(), emp.getSalario(), emp.getComision());
        }

        session.close();
    }
}
