import Model.DepartamentosEntity;
import org.hibernate.*;

public class Departamentos_CRUD {

    public static void insertDep(SessionFactory sessionFactory, int dept_no, String dnombre, String loc){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        DepartamentosEntity dep = new DepartamentosEntity();
        dep.setDeptNo(dept_no);
        dep.setDnombre(dnombre);
        dep.setLoc(loc);
        session.save(dep);

        tx.commit(); // valida la transaccion
        session.close();
    }

    public static void updateDepName(SessionFactory sessionFactory, int dept_no, String newName){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            DepartamentosEntity dep = (DepartamentosEntity) session.load(DepartamentosEntity.class, dept_no);
            System.out.printf("Nom antic: %s%n", dep.getDnombre());
            dep.setDnombre(newName);
            session.update(dep); // modifica el departament a la taula
            tx.commit();
            System.out.printf("Nom nou: %s%n", dep.getDnombre());
        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTE EL DEPARTAMENTO...");
        } catch (Exception e) {
            System.out.println("ERROR NO CONTROLADO....");
            e.printStackTrace();
        }

        session.close();
    }
}
