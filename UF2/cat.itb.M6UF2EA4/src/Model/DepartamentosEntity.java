package Model;

import java.util.Collection;
import java.util.Objects;

public class DepartamentosEntity {
    private int deptNo;
    private String dnombre;
    private String loc;
    private Collection<EmpleadosEntity> empleadosByDeptNo;

    public int getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(int deptNo) {
        this.deptNo = deptNo;
    }

    public String getDnombre() {
        return dnombre;
    }

    public void setDnombre(String dnombre) {
        this.dnombre = dnombre;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartamentosEntity that = (DepartamentosEntity) o;
        return deptNo == that.deptNo && Objects.equals(dnombre, that.dnombre) && Objects.equals(loc, that.loc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptNo, dnombre, loc);
    }

    public Collection<EmpleadosEntity> getEmpleadosByDeptNo() {
        return empleadosByDeptNo;
    }

    public void setEmpleadosByDeptNo(Collection<EmpleadosEntity> empleadosByDeptNo) {
        this.empleadosByDeptNo = empleadosByDeptNo;
    }

    @Override
    public String toString() {
        return "DepartamentosEntity{" +
                "deptNo=" + deptNo +
                ", dnombre='" + dnombre + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }
}
