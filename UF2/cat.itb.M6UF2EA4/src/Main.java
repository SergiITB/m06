import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.Scanner;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        final Session session = getSession();

        System.out.print("      ---------MENU--------" +
                "\n1) Exercici 1" +
                "\n2) Exercici 2" +
                "\n3) Exercici 3" +
                "\n4) Exercici 4" +
                "\n5) Exercici 5" +
                "\n6) Exercici 6" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                ex1();
                break;
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
            case 4:
                ex4();
                break;
            case 5:
                ex5();
                break;
            case 6:
                ex6();
                break;
        }
    }
    private static void ex6() {
        Empleados_CRUD.consultationWithMinSalary(ourSessionFactory,2000.00);
    }

    private static void ex5() {
        Empleados_CRUD.deleteEmpByName(ourSessionFactory,"ARROYO");
    }

    private static void ex4() {
        Empleados_CRUD.updateEmpSal(ourSessionFactory,7499,2100.00);
    }

    private static void ex3() {
        Departamentos_CRUD.updateDepName(ourSessionFactory,20,"RECERCA");
    }

    private static void ex2() {
        Empleados_CRUD.insertEmp(ourSessionFactory,8744,"FERNANDEZ","ANALISTA",8745,"2021-08-21",3000.00,0.00,60);
        Empleados_CRUD.insertEmp(ourSessionFactory,9734,"SANCHEZ","VENDEDOR",9833,"2021-09-11",2500.00,2.00,70);
    }

    private static void ex1() {
       Departamentos_CRUD.insertDep(ourSessionFactory,60,"TECNOLOGIA","BARCELONA");
       Departamentos_CRUD.insertDep(ourSessionFactory,70,"INFORMATICA","SEVILLA");
    }
}