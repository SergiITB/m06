import Connexio.ConnectionMongoCluster;
import Pojo.Person;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCommandException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.addToSet;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("---------MENU--------" +
                "\n1) Exercici 1: Carregar fitxers de documents"+
                "\n2) Exercici 2: Carregar un fitxer que conté un array de documents amb la llibreria \"Gson\" "+
                "\n3) Exercici 3: Actualitzar documents"+
                "\n4) Exercici 4: Cerques"+
                "\n5) Exercici 5: Eliminar documents"+
                "\n6) Exercici 6: Eliminar una collection"+
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                carregaDocuments("itb", "products2","cat.itb.M6UF3EA2/products.json");
                carregaDocuments("itb", "students2","cat.itb.M6UF3EA2/students.json");
                break;
            case 2:
                carregaDocumentsArray();
                break;
            case 3:
                actualitzarStockProductes();
                afegirCampDiscount();
                afegirCategoria();
                break;
            case 4:
                mostrarInfoEstudiantPerDNI();
                mostrarInfoEstudiantPerDataNaixement();
                mostrarAmicsEstudiantPerNom();
                break;
            case 5:
                eliminarProductesRangPreu();
                eliminarEstudiantPerNom();
                break;
            case 6:
                dropCollection("itb", "people2");
                comprovarExistenciaCollection();
                break;
        }
    }

    private static void comprovarExistenciaCollection() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoIterable <String> collection =  db.listCollectionNames();
        for(String s : collection) {
            if(s.equals("people2")) {
                System.out.println("La colecció existeix");
                System.exit(0);
            }
        }
        System.out.println("La colecció no existeix");
    }

    private static void dropCollection(String database, String collec) {
        System.out.println(" A) Eliminar colecció.");
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        long numDocuments = db.getCollection(collec).countDocuments();
        System.out.println("Num documents colecció "+collec+": "+numDocuments);
        db.getCollection(collec).drop();
    }

    private static void eliminarEstudiantPerNom() {
        System.out.println(" B) Elimina l’estudiant anomenat Daniel Vintro.");
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("students2");
        collection.deleteOne(and(eq("firstname","Daniel"),eq("lastname1","Vintro")));
        ConnectionMongoCluster.closeConnection();
    }

    private static void eliminarProductesRangPreu() {
        System.out.println(" A) Esborrar els productes que valen entre 400 i 600.");
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("products2");
        DeleteResult deleteResultCompany = collection.deleteMany(and(gte("price",400),lte("price",600)));
        System.out.println("Num Productes eliminats: "+deleteResultCompany.getDeletedCount());
        ConnectionMongoCluster.closeConnection();
    }

    private static void mostrarAmicsEstudiantPerNom() {
        MongoDatabase sampleTrainingDB = ConnectionMongoCluster.getDatabase("itb");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("people2").find(new Document("name", "Jessica Adamson")).projection(Projections.include("friends"));
        System.out.println("************** Amics de la persona Jessica Adamson **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));
    }

    private static void mostrarInfoEstudiantPerDataNaixement() {
        MongoDatabase sampleTrainingDB = ConnectionMongoCluster.getDatabase("itb");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("students2").find(new Document("birth_year", 1984));
        System.out.println("************** Estudiants nascuts l’any 1984. **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));
    }

    private static void mostrarInfoEstudiantPerDNI() {
        MongoDatabase sampleTrainingDB = ConnectionMongoCluster.getDatabase("itb");
        FindIterable<Document> listGrades;
        listGrades = sampleTrainingDB.getCollection("students2").find(new Document("dni", "07517306D")).projection(Projections.include("email","phone","phone_aux"));
        System.out.println("************** Correu electrònic i els telèfons principals i auxiliar de l’estudiant amb\"dni\":\"07517306D\". **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));
    }

    private static void afegirCategoria() {
        MongoDatabase myDB = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("products2");
        System.out.println(" C) Afegir una nova categoria anomenada smartTV al producte anomenat Apple TV.");
        UpdateResult updateResult = collection.updateOne(eq("name","Apple TV"), addToSet("categories", "smartTV"));
        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");
    }

    private static void afegirCampDiscount() {
        MongoDatabase myDB = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("products2");
        System.out.println(" B) Afegir nou camp discount amb valor 0.20 als productes que el seu Stock sigui més gran de 100.");
        UpdateResult updateResult = collection.updateMany(gt("stock",100), set("discount", 0.20));
        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");
    }

    private static void actualitzarStockProductes() {
        MongoDatabase myDB = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("products2");
        System.out.println(" A) Update stock a 150 quan preu estigui entre 600 i 1000");
        UpdateResult updateResult = collection.updateMany(and(gte("price",600),lte("price",1000)), set("stock", 150));
        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");
    }

    private static void carregaDocumentsArray() {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));


        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader("cat.itb.M6UF3EA2/people.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Person> collectionPeople = db.getCollection("people2",Person.class);
        collectionPeople.insertMany(Arrays.asList(people));
        for (Person pers:people) {
            System.out.println(pers);
        }
        ConnectionMongoCluster.closeConnection();
    }

    public static void carregaDocuments(String database, String collection, String file){
        List<Document> productes = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                productes.add(Document.parse(linea));
                System.out.println(collection+": "+ linea);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        MongoCollection<Document> collectionProducts = db.getCollection(collection);
        collectionProducts.insertMany(productes);
        ConnectionMongoCluster.closeConnection();
    }
}
