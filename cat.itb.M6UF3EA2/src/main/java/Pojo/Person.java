package Pojo;

import java.io.Serializable;
import java.util.ArrayList;

public class Person implements Serializable {

    boolean isActive;
    String balance;
    String picture;
    int age;
    String name;
    String company;
    String phone;
    String email;
    String address;
    String about;
    String registered;
    Double latitude;
    ArrayList<Friend> friends;
    String gender;
    String randomArrayItem;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public ArrayList<Friend> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<Friend> friends) {
        this.friends = friends;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRandomArrayItem() {
        return randomArrayItem;
    }

    public void setRandomArrayItem(String randomArrayItem) {
        this.randomArrayItem = randomArrayItem;
    }

    ArrayList<String> tags;

    public Person(boolean isActive, String balance, String picture, int age, String name, String company, String phone, String email, String address, String about, String registered, Double latitude, ArrayList<String> tags, ArrayList<Friend> friends, String gender, String randomArrayItem) {
        this.isActive = isActive;
        this.balance = balance;
        this.picture = picture;
        this.age = age;
        this.name = name;
        this.company = company;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.about = about;
        this.registered = registered;
        this.latitude = latitude;
        this.tags = tags;
        this.friends = friends;
        this.gender = gender;
        this.randomArrayItem = randomArrayItem;
    }

    @Override
    public String toString() {
        return "Person{" +
                "isActive=" + isActive +
                ", balance='" + balance + '\'' +
                ", picture='" + picture + '\'' +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", company='" + company + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", about='" + about + '\'' +
                ", registered='" + registered + '\'' +
                ", latitude=" + latitude +
                ", friends=" + friends +
                ", gender='" + gender + '\'' +
                ", randomArrayItem='" + randomArrayItem + '\'' +
                ", tags=" + tags +
                '}';
    }
}