package Pojo;

import java.io.Serializable;

public class Friend implements Serializable {

    int id;
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Friend(int id, String name) {
        this.id = id;
        this.name = name;
    }
}