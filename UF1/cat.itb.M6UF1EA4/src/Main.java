import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.print("---------MENU--------" +
                "\n1) Exercici 1 (Crear classe Empleat i classe Departament)" +
                "\n2) Exercici 2 (Escriure 5 objectes Empleat i 5 objectes Departament)" +
                "\n3) Exercici 3 (LLegeix el 2 fitxers)" +
                "\n4) Exercici 4 (Afegir 5 objectes Empleat i 5 objectes Departament)" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                new ex1();
                break;
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
            case 4:
                ex4();
                break;
        }
    }
    private static void ex4() throws IOException {
        File fitxerDepartaments = new File("FitxerDepartaments.dat");
        ObjectOutputStream departamentsDataOS;
        if (!fitxerDepartaments.exists())
        { //Si el fichero no existe crea un ObjectOutputStream, la primera vez
            FileOutputStream fileout;
            fileout = new FileOutputStream(fitxerDepartaments);
            departamentsDataOS = new ObjectOutputStream(fileout);}
        else
        { // Si ya existe el fichero creará un ObjectOutpuSream con el método writeStreamHeader redefinit (sense fer res)
            departamentsDataOS = new MiObjectOutputStream(new FileOutputStream(fitxerDepartaments,true));}
        ex1.Departament departament;
        int[] departamentsIds = {6,7,8,9,10};
        String[] nomsDepartaments = {"RecursosHumans","Comercial","Logistica","Gestio","Direccio"};
        String[] localitats = {"Ceuta","Tarragona","Melilla","Reus","Girona"};
        for (int i=0;i<departamentsIds.length; i++){
            departament= new ex1.Departament(departamentsIds[i], nomsDepartaments[i],localitats[i]);
            departamentsDataOS.writeObject(departament);
        }
        System.out.println("5 Objectes Departament escrits al fitxer \"FitxerDepartaments.dat\" !!");


        File fitxerEmpleats = new File("FitxerEmpleats.dat");
        ObjectOutputStream empleatsDataOS;
        if (!fitxerEmpleats.exists())
        { //Si el fichero no existe crea un ObjectOutputStream, la primera vez
            FileOutputStream fileout;
            fileout = new FileOutputStream(fitxerEmpleats);
            empleatsDataOS = new ObjectOutputStream(fileout);}
        else
        { // Si ya existe el fichero creará un ObjectOutpuSream con el método writeStreamHeader redefinit (sense fer res)
            empleatsDataOS = new MiObjectOutputStream(new FileOutputStream(fitxerEmpleats,true));}
        ex1.Empleat empleat;
        int[] empleatsIds = {6,7,8,9,10};
        String[] cognomsEmpleats = {"Santos","Martinez","Fernandez","Sanchez","Martin"};
        int[] departaments = {10,9,8,7,6};
        double[] salaris = {40.000,37.000,38.000,32.000,31.000};
        for (int i=0;i<empleatsIds.length; i++){
            empleat= new ex1.Empleat(empleatsIds[i], cognomsEmpleats[i],departaments[i],salaris[i]);
            empleatsDataOS.writeObject(empleat);
        }
        System.out.println("5 Objectes Empleat escrits al fitxer \"FitxerEmpleats.dat\" !!");
        ex3();
    }

    private static void ex3() throws IOException {
        System.out.println("FITXER DEPARTAMENTS:");
        ex1.Departament departament;
        ObjectInputStream departamemtsDataIS = new ObjectInputStream(new FileInputStream("FitxerDepartaments.dat"));
        int i = 1;
        try {
            while (true) {
                departament = (ex1.Departament) departamemtsDataIS.readObject();
                System.out.print(i + "=>");
                i++;
                System.out.printf("Id: %s, Nom: %s, Localitat: %s %n",
                        departament.getId(),departament.getNom(),departament.getLocalitat());
            }
        } catch (EOFException eo) {
            System.out.println("FI DE LA LECTURA !!\n");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        departamemtsDataIS.close();


        System.out.println("FITXER EMPLEATS:");
        ex1.Empleat empleat;
        ObjectInputStream empleatsDataIS = new ObjectInputStream(new FileInputStream("FitxerEmpleats.dat"));
        int count = 1;
        try {
            while (true) {
                empleat = (ex1.Empleat) empleatsDataIS.readObject();
                System.out.print(count + "=>");
                count++;
                System.out.printf("Id: %s, Cognom: %s, Departament: %d, Salari: %f %n",
                        empleat.getId(),empleat.getCognom(),empleat.getDepartament(),empleat.getSalari());
            }
        } catch (EOFException eo) {
            System.out.println("FI DE LA LECTURA !!\n");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        departamemtsDataIS.close();
    }

    private static void ex2() throws IOException {
        //Creem el fitxer de departaments i afegim 5 departaments
        ex1.Departament departament;
        FileOutputStream departamentsFileout = new FileOutputStream("FitxerDepartaments.dat");
        ObjectOutputStream departamentsDataOS = new ObjectOutputStream(departamentsFileout);
        int[] departamentsIds = {1,2,3,4,5};
        String[] nomsDepartaments = {"Contabilitat","Marketing","IT","Compres","Ventes"};
        String[] localitats = {"Barcelona","Madrid","Valencia","Sevilla","Canaries"};
        for (int i=0;i<departamentsIds.length; i++){
            departament= new ex1.Departament(departamentsIds[i], nomsDepartaments[i],localitats[i]);
            departamentsDataOS.writeObject(departament);
        }
        System.out.println("5 Objectes Departament escrits al fitxer \"FitxerDepartaments.dat\" !!");

        //Creem el fitxer d'empleats i afegim 5 empleats
        ex1.Empleat empleat;
        FileOutputStream empleatsFileout = new FileOutputStream("FitxerEmpleats.dat");
        ObjectOutputStream empleatsDataOS = new ObjectOutputStream(empleatsFileout);
        int[] empleatsIds = {1,2,3,4,5};
        String[] cognomsEmpleats = {"Garcia","Rodriguez","Gonzalez","Lopez","Gomez"};
        int[] departaments = {5,4,3,2,1};
        double[] salaris = {34.000,35.000,36.000,37.000,38.000};
        for (int i=0;i<empleatsIds.length; i++){
            empleat= new ex1.Empleat(empleatsIds[i], cognomsEmpleats[i],departaments[i],salaris[i]);
            empleatsDataOS.writeObject(empleat);
        }
        System.out.println("5 Objectes Empleat escrits al fitxer \"FitxerEmpleats.dat\" !!");
    }

    public static class ex1 {
        public static class Empleat implements Serializable{
            int id;
            String cognom;
            int departament;
            double salari;

            public Empleat(int id, String cognom, int departament, double salari) {
                this.id = id;
                this.cognom = cognom;
                this.departament = departament;
                this.salari = salari;
            }

            public void setId(int id) { this.id = id; }
            public void setCognom(String cognom) {this.cognom = cognom; }
            public void setDepartament(int departament) {this.departament = departament; }
            public void setSalari(double salari) {this.salari = salari; }

            public int getId() {return id; }
            public String getCognom() { return cognom; }
            public int getDepartament() { return departament; }
            public double getSalari() { return salari; }
        }

        public static class Departament implements Serializable {
            int id;
            String nom;
            String localitat;

            public Departament(int id, String nom, String localitat) {
                this.id = id;
                this.nom = nom;
                this.localitat = localitat;
            }

            public void setId(int id) { this.id = id; }
            public void setNom(String nom) { this.nom = nom; }
            public void setLocalitat(String localitat) { this.localitat = localitat; }

            public int getId() { return id; }
            public String getNom() { return nom; }
            public String getLocalitat() { return localitat; }
        }
    }

    public static class MiObjectOutputStream extends ObjectOutputStream
    {
        /** Constructor que recibe OutputStream */
        public MiObjectOutputStream(OutputStream out) throws IOException
        { super(out); }
        /** Constructor sin parámetros */
        protected MiObjectOutputStream() throws IOException, SecurityException
        { super();}
        /** Redefinición del método de escribir la cabecera para que no haga nada. */
        protected void writeStreamHeader() throws IOException
        { }
    }
}
