import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.print("---------MENU--------" +
                "\n1) Exercici 1" +
                "\n2) Exercici 2" +
                "\n3) Exercici 3" +
                "\n4) Exercici 4" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                ex1();
                break;
            case 2:
                ex2(scanner);
                break;
            case 3:
                ex3(scanner);
                break;
            case 4:
                ex4();
                break;
        }
    }

    private static void ex4() throws IOException {
        DataInputStream dataIS = new DataInputStream(new FileInputStream("Departaments.dat"));
        try {
            while (true) {
                int id = dataIS.readInt();
                String nom = dataIS.readUTF();
                String localitat = dataIS.readUTF();
                System.out.printf("ID: %s  NOM: %s  LOCALITAT:%s\n",id,nom,localitat);
            }
        }catch (EOFException eo) {}
        dataIS.close();
    }

    private static void ex3(Scanner scanner) throws IOException {
        System.out.print("Introdueix un numero de departament: ");
        int inputId = scanner.nextInt();
        DataInputStream dataIS = new DataInputStream(new FileInputStream("Departaments.dat"));
        List<Integer> ids = new ArrayList<>();
        List<String> noms = new ArrayList<>();
        List<String> localitats = new ArrayList<>();

        try {
            while (true) {
                int id = dataIS.readInt();
                String nom = dataIS.readUTF();
                String localitat = dataIS.readUTF();
                ids.add(id);
                noms.add(nom);
                localitats.add(localitat);
            }
        }catch (EOFException eo) {}
        dataIS.close();

        int count = 0;
        for (int id:ids) {
            if (id == inputId){
                ids.remove(inputId);
                noms.remove(inputId);
                localitats.remove(inputId);
            }
            else ++count;
        }
        if (count==ids.size()){
            System.out.println("AQUEST DEPARTAMENT NO EXISTEIX!!!");
        }


        File fichero = new File("Departaments.dat");
        DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(fichero));
        for (int i=0;i<ids.size(); i++){
            dataOS.writeInt(ids.get(i));
            dataOS.writeUTF(noms.get(i));
            dataOS.writeUTF(localitats.get(i));
        }
    }

    private static void ex2(Scanner scanner) throws IOException {
        System.out.print("Introdueix un numero de departament: ");
        int inputId = scanner.nextInt();
        System.out.print("Introdueix el nom que vols que tingui: ");
        String inputName = scanner.next();
        System.out.print("Introdueix la localitat que vols que tingui: ");
        String inputLocalitat = scanner.next();
        DataInputStream dataIS = new DataInputStream(new FileInputStream("Departaments.dat"));
        List<Integer> ids = new ArrayList<>();
        List<String> noms = new ArrayList<>();
        List<String> localitats = new ArrayList<>();

        try {
            while (true) {
                int id = dataIS.readInt();
                String nom = dataIS.readUTF();
                String localitat = dataIS.readUTF();
                ids.add(id);
                noms.add(nom);
                localitats.add(localitat);
            }
        }catch (EOFException eo) {}
        dataIS.close();

        int count = 0;
        for (int id:ids) {
            if (id == inputId){
                noms.set(--inputId,inputName);
                localitats.set(inputId,inputLocalitat);
            }
            else ++count;
        }
        if (count==ids.size()){
            System.out.println("AQUEST DEPARTAMENT NO EXISTEIX!!!");
        }


        File fichero = new File("Departaments.dat");
        DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(fichero));
        for (int i=0;i<ids.size(); i++){
            dataOS.writeInt(ids.get(i));
            dataOS.writeUTF(noms.get(i));
            dataOS.writeUTF(localitats.get(i));
        }
    }

    private static void ex1() throws IOException {
        File fichero = new File("Departaments.dat");
        DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(fichero));
        int[] departaments = {1,2,3,4,5,6,7,8,9,10};
        String[] nomsDepartaments = {"Contabilitat","Marketing","IT","Compres","Ventes","AtencioClient","RH","Marketing","Logistica","Direcció"};
        String[] localitats = {"Barcelona","Madrid","Valencia","Sevilla","Canaries","Mallorca","Asturies","Valencia","Barcelona","Ibiza"};
        for (int i=0;i<departaments.length; i++){
            dataOS.writeInt(departaments[i]);
            dataOS.writeUTF(nomsDepartaments[i]);
            dataOS.writeUTF(localitats[i]);
        }
        dataOS.close();
    }
}
