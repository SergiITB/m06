import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.print("---------MENU--------" +
                "\n1) Exercici 1" +
                "\n2) Exercici 2" +
                "\n3) Exercici 3" +
                "\n4) Exercici 4" +
                "\n5) Exercici 5" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                ex1(scanner);
                break;
            case 2:
                ex2(scanner);
                break;
            case 3:
                ex3(scanner);
                break;
            case 4:
                ex4(scanner);
                break;
            case 5:
                ex5();
                break;
        }
    }
    private static void ex5() {

    }

    private static void ex4(Scanner scanner) throws IOException {
        try {
            System.out.print("Introdueix un ID d'empleat que vols eliminar: ");
            int inputId = scanner.nextInt();

            StringBuffer buffer = null;
            File fichero = new File("AleatorioEmple.dat");
            RandomAccessFile file = new RandomAccessFile(fichero, "rw");
            int posicion = (inputId - 1) * 36;
            file.seek(posicion);
            int id = file.readInt();
            if (id == inputId) {
                file.seek(posicion);
                file.writeInt(-1);
                buffer = new StringBuffer(id);
                buffer.setLength(10);
                file.writeChars(buffer.toString());
                file.writeInt(0);
                file.writeDouble(0);
            }
            file.close();
        }catch (EOFException e){
            System.out.println("EMPLEAT NO EXISTEIX");
            e.printStackTrace();
        }
    }

    private static void ex3(Scanner scanner) throws IOException {
        System.out.print("Introdueix un ID d'empleat: ");
        int inputId = scanner.nextInt();
        System.out.print("Introdueix el salari que vols que tingui: ");
        double inputSalary = scanner.nextDouble();

        StringBuffer buffer = null;
        File fichero = new File("AleatorioEmple.dat");
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");
        int posicion = (inputId - 1) * 36;
        file.seek(posicion);
        if (posicion <= file.length()) {
            posicion = posicion + 4 +20+4;
            file.seek(posicion);
            file.writeDouble(inputSalary);
        }else System.out.println("L'EMPLEAT NO EXISTEIX !!");
        file.close();
    }

    private static void ex2(Scanner scanner) throws IOException {
        System.out.println("Introdueix les dades de l'empleat a crear:");
        System.out.print("ID: ");
        int inputId = scanner.nextInt();
        System.out.print("Cognom: ");
        String apellido = scanner.next();
        System.out.print("Departament: ");
        int dep = scanner.nextInt();
        System.out.print("Salari: ");
        double salario = scanner.nextDouble();

        StringBuffer buffer = null;
        File fichero = new File("AleatorioEmple.dat");
        //declara el fichero de acceso aleatorio
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");
        int posicion = (inputId-1)*36;
        file.seek(posicion); //nos posicionamos en posicion
        if(posicion>=file.length()) {
            file.seek(file.length());
            file.writeInt(inputId); //identificar empleado
            buffer = new StringBuffer(apellido);
            buffer.setLength(10); //10 caracteres para el apellido
            file.writeChars(buffer.toString());//insertar apellido
            file.writeInt(dep);       //insertar departamento
            file.writeDouble(salario);//insertar salario;

        }else{
            System.out.println("AQUEST USUARI YA EXISTEIX !!");
        }
        file.close();
    }

    private static void ex1(Scanner scanner) throws IOException {
        System.out.print("Introdueix l'identificador de l'empleat que vols consultar: ");
        int inputId = scanner.nextInt();

        File fichero = new File("AleatorioEmple.dat");
        //declara el fichero de acceso aleatorio
        RandomAccessFile file = new RandomAccessFile(fichero, "r");
        //
        int  id, dep, posicion;
        double salario;
        char[] apellido = new char[10];
        char aux;

        posicion = (inputId-1)*36;  //para situarnos al principio

        file.seek(posicion); //nos posicionamos en posicion
        id = file.readInt();   // obtengo id de empleado

        //recorro uno a uno los caracteres del apellido
        for (int i = 0; i < apellido.length; i++) {
            aux = file.readChar();
            apellido[i] = aux;    //los voy guardando en el array
        }

        //convierto a String el array
        String apellidos = new String(apellido);
        dep = file.readInt();        //obtengo dep
        salario = file.readDouble(); //obtengo salario

        if(id == inputId)
            System.out.printf("ID: %s, Apellido: %s, Departamento: %d, Salario: %.2f %n",
                    id, apellidos.trim(), dep, salario);
        else System.out.println("NO EXISTEIX");

        file.close();  //cerrar fichero
    }
}
