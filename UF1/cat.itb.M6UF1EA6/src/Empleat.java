import java.io.Serializable;

public class Empleat implements Serializable {
    int id;
    String cognom;
    int departament;
    double salari;

    public Empleat(int id, String cognom, int departament, double salari) {
        this.id = id;
        this.cognom = cognom;
        this.departament = departament;
        this.salari = salari;
    }

    public void setId(int id) { this.id = id; }
    public void setCognom(String cognom) {this.cognom = cognom; }
    public void setDepartament(int departament) {this.departament = departament; }
    public void setSalari(double salari) {this.salari = salari; }

    public int getId() {return id; }
    public String getCognom() { return cognom; }
    public int getDepartament() { return departament; }
    public double getSalari() { return salari; }
}