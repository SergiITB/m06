import com.thoughtworks.xstream.XStream;

import java.io.*;

public class EscribirEmpleats {
    public static void escribirEmpleats() throws IOException, ClassNotFoundException {
        File fichero = new File("cat.itb.M6UF1EA6/FitxerEmpleats.dat");
        FileInputStream filein = new FileInputStream(fichero);//flujo de entrada
        ObjectInputStream dataIS = new ObjectInputStream(filein); //conecta el flujo de bytes al flujo de datos

        System.out.println("Comienza el proceso de creación del fichero a XML ...");

        ListaEmpleats listaemp = new ListaEmpleats();//Creamos un objeto Lista de Empleats
        try {
            while (true) { //lectura del fichero
                Empleat empleat = (Empleat) dataIS.readObject();//leer un Empleat
                listaemp.add(empleat); //añadir empleat a la lista
            }
        } catch (EOFException eo) {}
        dataIS.close(); //cerrar stream de entrada
        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaEmpleats", ListaEmpleats.class);
            xstream.alias("DatosEmpleat", Empleat.class);

            //Insertar los objetos en el XML
            xstream.toXML(listaemp, new FileOutputStream("Empleats.xml"));
            System.out.println("FITXER XML CREAT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
