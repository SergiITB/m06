import com.thoughtworks.xstream.XStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class LeerDepartaments {
    public static void leerDepartaments() throws FileNotFoundException {
        XStream xstream = new XStream();

        xstream.alias("ListaDepartaments", ListaDepartaments.class);
        xstream.alias("DatosDepartament", Departament.class);
//        xstream.addImplicitCollection(ListaDepartaments.class, "listaDep");

        ListaDepartaments listadoTodas = (ListaDepartaments)
                xstream.fromXML(new FileInputStream("Departaments.xml"));
        System.out.println("Numero de Departaments: " +
                listadoTodas.getListaDepartaments().size());

        List<Departament> listaDepartaments;
        listaDepartaments = listadoTodas.getListaDepartaments();

        for (Departament d : listaDepartaments) {
            System.out.printf("ID: %d, Nom: %s, Localitat: %s %n",
                    d.getId(), d.getNom(), d.getLocalitat());
        }
        System.out.println("Fin de listado .....");
    }
}
