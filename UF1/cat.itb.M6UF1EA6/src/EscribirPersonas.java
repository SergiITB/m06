import com.thoughtworks.xstream.XStream;

import java.io.*;

public class EscribirPersonas {
    public static void escribirPersonas () throws IOException, ClassNotFoundException {
        File fichero = new File("cat.itb.M6UF1EA6/FichPersona.dat");
        FileInputStream filein = new FileInputStream(fichero);//flujo de entrada
        //conecta el flujo de bytes al flujo de datos
        ObjectInputStream dataIS = new ObjectInputStream(filein);
        System.out.println("Comienza el proceso de creación del fichero a XML ...");

        ListaPersonas listaper = new ListaPersonas();//Creamos un objeto Lista de Personas
        try {
            while (true) { //lectura del fichero
                Persona persona= (Persona) dataIS.readObject();//leer una Persona
                listaper.add(persona); //añadir persona a la lista
            }
        }catch (EOFException eo) {}
        dataIS.close(); //cerrar stream de entrada

        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaPersonasMunicipio", ListaPersonas.class);
            xstream.alias("DatosPersona", Persona.class);
            //quitar etiqueta lista (atributo de la clase ListaPersonas)
            xstream.addImplicitCollection(ListaPersonas.class, "lista");
            //Insertar los objetos en el XML
            xstream.toXML(listaper, new FileOutputStream("Personas.xml"));
            System.out.println("FITXER XML CREAT");

        }catch (Exception e)
        {e.printStackTrace();}
    }
}
