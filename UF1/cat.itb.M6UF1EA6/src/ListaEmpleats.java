import java.util.ArrayList;
import java.util.List;

public class ListaEmpleats {
    private final List<Empleat> listaEmp = new ArrayList<Empleat>();

    public ListaEmpleats(){
    }

    public void add(Empleat emp) {
        listaEmp.add(emp);
    }

    public List<Empleat> getListaEmpleats() {
        return listaEmp;
    }
}
