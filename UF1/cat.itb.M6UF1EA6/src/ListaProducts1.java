import java.util.ArrayList;
import java.util.List;

public class ListaProducts1 {
    private List<Product1> lista = new ArrayList<Product1>();

    public ListaProducts1(){
    }

    public void add(Product1 pro) {
        lista.add(pro);
    }

    public List<Product1> getListaProducts1() {
        return lista;
    }
}
