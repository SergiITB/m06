import java.util.ArrayList;
import java.util.List;

public class ListaDepartaments {
    private final List<Departament> listaDep = new ArrayList<Departament>();

    public ListaDepartaments(){
    }

    public void add(Departament dep) {
        listaDep.add(dep);
    }

    public List<Departament> getListaDepartaments() {
        return listaDep;
    }
}
