import com.thoughtworks.xstream.XStream;

import java.io.*;

public class EscribirProducts1 {
    public static void escribirProducts1() throws IOException, ClassNotFoundException {
        File fichero = new File("cat.itb.M6UF1EA6/Products1.dat");
        FileInputStream filein = new FileInputStream(fichero);//flujo de entrada
        //conecta el flujo de bytes al flujo de datos
        ObjectInputStream dataIS = new ObjectInputStream(filein);
        System.out.println("Comienza el proceso de creación del fichero a XML ...");

        ListaProducts1 listapro = new ListaProducts1();//Creamos un objeto Lista de Products1
        try {
            while (true) { //lectura del fichero
                Product1 product1= (Product1) dataIS.readObject();//leer una Product1
                listapro.add(product1);//añadir product1 a la lista
            }
        }catch (EOFException eo) {}
        dataIS.close(); //cerrar stream de entrada
        try {

            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaProducts1", ListaProducts1.class);
            xstream.alias("DatosProduct1", Product1.class);
            //Insertar los objetos en el XML
            xstream.toXML(listapro, new FileOutputStream("Products1.xml"));
            System.out.println("FITXER XML CREAT");

        }catch (Exception e)
        {e.printStackTrace();}
    }
}
