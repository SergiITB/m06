import java.io.Serializable;

public class Departament implements Serializable {
    int id;
    String nom;
    String localitat;

    public Departament(int id, String nom, String localitat) {
        this.id = id;
        this.nom = nom;
        this.localitat = localitat;
    }

    public void setId(int id) { this.id = id; }
    public void setNom(String nom) { this.nom = nom; }
    public void setLocalitat(String localitat) { this.localitat = localitat; }

    public int getId() { return id; }
    public String getNom() { return nom; }
    public String getLocalitat() { return localitat; }
}
