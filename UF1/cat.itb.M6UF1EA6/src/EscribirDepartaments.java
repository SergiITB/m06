import com.thoughtworks.xstream.XStream;

import java.io.*;

public class EscribirDepartaments {
    public static void escribirDepartaments() throws IOException, ClassNotFoundException{
        File fichero = new File("cat.itb.M6UF1EA6/FitxerDepartaments.dat");
        FileInputStream filein = new FileInputStream(fichero);//flujo de entrada
        ObjectInputStream dataIS = new ObjectInputStream(filein); //conecta el flujo de bytes al flujo de datos

        System.out.println("Comienza el proceso de creación del fichero a XML ...");

        ListaDepartaments listadep = new ListaDepartaments();//Creamos un objeto Lista de Departaments
        try {
            while (true) { //lectura del fichero
                Departament departament = (Departament) dataIS.readObject();//leer un Departament
                listadep.add(departament); //añadir departament a la lista
            }
        } catch (EOFException eo) {
        }
        dataIS.close(); //cerrar stream de entrada
        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaDepartaments", ListaDepartaments.class);
            xstream.alias("DatosDepartament", Departament.class);
            //Insertar los objetos en el XML
            xstream.toXML(listadep, new FileOutputStream("Departaments.xml"));
            System.out.println("FITXER XML CREAT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
