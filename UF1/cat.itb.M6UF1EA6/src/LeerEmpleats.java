import com.thoughtworks.xstream.XStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class LeerEmpleats {
    public static void leerEmpleats() throws FileNotFoundException {
        XStream xstream = new XStream();

        xstream.alias("ListaEmpleats", ListaEmpleats.class);
        xstream.alias("DatosEmpleat", Empleat.class);
//        xstream.addImplicitCollection(ListaEmpleats.class, "listaEmp");

        ListaEmpleats listadoTodas = (ListaEmpleats)
                xstream.fromXML(new FileInputStream("Empleats.xml"));
        System.out.println("Numero de Empleats: " +
                listadoTodas.getListaEmpleats().size());

        List<Empleat> listaEmpleats;
        listaEmpleats = listadoTodas.getListaEmpleats();

        for (Empleat e : listaEmpleats) {
            System.out.printf("ID: %s, Cognom: %s, Departament: %d, Salari: %s %n",
                    e.getId(), e.getCognom(),e.getDepartament(),e.getSalari());
        }
        System.out.println("Fin de listado .....");
    }
}
