import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //List<Persona> lista = new ArrayList<Persona>();

        System.out.print("---------MENU--------" +
                "\n1) Exercici 1 (Crear fitxer Personas.xml a partir de fitxer binari i llegir fitxer XML)" +
                "\n2) Exercici 2 (Crear fitxers Empleats.xml i Departaments.xml a partir de fitxers binaris)" +
                "\n3) Exercici 3 (Llegir els fitxers Empleats.xml i Departaments.xml)" +
                "\n4) Exercici 4 (Llegir fitxer products1.json i crear un fitxer binari Products1.dat amb les dades obtingudes)" +
                "\n5) Exercici 5 (Crear el fitxer Products1.xml a partir dels objectes del fitxer binari Products1.dat)" +
                "\n6) ***EXTRA***  Exercici 6 (Llegir el fitxer binari Products1.dat)" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                ex1();
                break;
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
            case 4:
                ex4();
                break;
            case 5:
                ex5();
                break;
            case 6:
                ex6();
                break;
        }
    }

    private static void ex6() throws IOException {
        System.out.println("FITXER PRODUCTS1:");
        Product1 product1;
        ObjectInputStream products1DataIS = new ObjectInputStream(new FileInputStream("cat.itb.M6UF1EA6/Products1.dat"));
        try {
            while (true) {
                product1 = (Product1) products1DataIS.readObject();
                System.out.printf("Name: %s, Price: %d, Stock: %d, Picture: %s, Categories: %s %n",
                       product1.getName(), product1.getPrice(), product1.getStock(), product1.getPicture(), product1.getCategories());
            }
        } catch (EOFException eo) {
            System.out.println("FI DE LA LECTURA !!\n");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        products1DataIS.close();
    }

    private static void ex5() throws IOException, ClassNotFoundException {
        EscribirProducts1.escribirProducts1();
    }

    private static void ex4() throws IOException{
        List<Product1> products = new ArrayList<>();
        llegirFitxerJSON(products);
        crearFitxerBinari(products);
    }

    private static void ex3() throws FileNotFoundException {
        System.out.println("EMPLEATS:\n");
        LeerEmpleats.leerEmpleats();
        System.out.println("\nDEPARTAMENTS:\n");
        LeerDepartaments.leerDepartaments();
    }

    private static void ex2() throws IOException, ClassNotFoundException {
        EscribirEmpleats.escribirEmpleats();
        EscribirDepartaments.escribirDepartaments();
    }

    private static void ex1() throws IOException, ClassNotFoundException {
        System.out.print("---------MENU--------" +
                "\n1) Crear fitxer Personas.xml" +
                "\n2) LLegir fitxer Personas.xml" +
                "\n\nINTRODUEIX L'APARTAT A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                EscribirPersonas.escribirPersonas();
                break;
            case 2:
                LeerPersonas.leerPersonas();
                break;
        }
    }


    private static void crearFitxerBinari(List<Product1> products) throws IOException {
        File fichero = new File("cat.itb.M6UF1EA6/Products1.dat");//declara el fichero
        FileOutputStream fileout = new FileOutputStream(fichero,true);  //crea el flujo de salida
        //conecta el flujo de bytes al flujo de datos
        ObjectOutputStream dataOS = new ObjectOutputStream(fileout);
        for (Product1 product : products) { //recorro los arrays
            dataOS.writeObject(product); //escribo la persona en el fichero
        }
        System.out.println("GRABADOS LOS DATOS DE LOS PRODUCTOS !!");
        dataOS.close();  //cerrar stream de salida
    }

    private static void llegirFitxerJSON(List <Product1> products) {
        // Creo una instancia de GSON
        Gson gson = new Gson();
        try (BufferedReader br = new BufferedReader(new FileReader("cat.itb.M6UF1EA6/products1.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                Product1 product1 = gson.fromJson(linea, Product1.class);
                products.add(product1);
            }

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}


