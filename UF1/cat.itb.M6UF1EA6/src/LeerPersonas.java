import com.thoughtworks.xstream.XStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class LeerPersonas {
    public static void leerPersonas() throws FileNotFoundException {
        XStream xstream = new XStream();

        xstream.alias("ListaPersonasMunicipio", ListaPersonas.class);
        xstream.alias("DatosPersona", Persona.class);
        xstream.addImplicitCollection(ListaPersonas.class, "lista");

        ListaPersonas listadoTodas = (ListaPersonas)
                xstream.fromXML(new FileInputStream("Personas.xml"));
        System.out.println("Numero de Personas: " +
                listadoTodas.getListaPersonas().size());

        List<Persona> listaPersonas;
        listaPersonas = listadoTodas.getListaPersonas();

        for (Persona p : listaPersonas) {
            System.out.printf("Nombre: %s, edad: %d %n",
                    p.getNombre(), p.getEdad());
        }
        System.out.println("Fin de listado .....");
    }
}
