import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.print("---------MENU--------\n1) Exercici 1" +
                "\n2) Exercici 2" +
                "\n3) Exercici 3" +
                "\n4) Exercici 4" +
                "\n5) Exercici 5" +
                "\n6) Exercici 6"+
                "\n7) Exercici 7"+
                "\n8) Exercici 8"+
                "\n9) Exercici 9"+
                "\n10) Exercici 10"+
                "\n11) Exercici 11"+
                "\n12) Exercici 12"+
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1 :
                ex1(scanner);
                break;
            case 2 :
                ex2(scanner);
                break;
            case 3 :
                ex3(scanner);
                break;
            case 4 :
                ex4();
                break;
            case 5 :
                ex5();
                break;
            case 6 :
                ex6();
                break;
            case 7 :
                ex7(scanner);
                break;
            case 8 :
                ex8(scanner);
                break;
            case 9 :
                ex9(scanner);
                break;
            case 10 :
                ex10();
                break;
            case 11 :
                ex11();
                break;
            case 12 :
                ex12();
                break;
        }
    }

    private static void ex12() {
    }

    private static void ex11() {
    }

    private static void ex10() throws FileNotFoundException {
        String fileName = "C:\\Users\\User\\Desktop\\enters.txt";
        Scanner scan = new Scanner(new File(fileName));
        //FALTA ACABAR
    }

    private static void ex9(Scanner scanner) {
        System.out.println("Introdueix el directori a comprovar: ");
        String path = scanner.next();
        File file = new File(path);
        if (file.exists()){
            System.out.println("El fixer existeis");
        }else System.out.println("El fitxer no existeis");
        if (file.isDirectory()){
            System.out.println("El fitxer es un directori");
        }else System.out.println("El fitxer no es un directori");
        if (file.isFile()){
            System.out.printf("Nom: %s\nMida: %s\nPermisos de lectura: %s\nPermisos d'escriptura: %s"
                    , file.getName(),file.getTotalSpace(),file.canRead(),file.canWrite());
        }
    }

    private static void ex8(Scanner scanner) {
        System.out.println("Introdueix el directori del fitxer a eliminar: ");
        String path = scanner.next();
        File file = new File(path);
        if (!file.delete()) {
            System.out.println("ERROR");
            ex8(scanner);
        }else{
            System.out.println("S'ha eliminat el fitxer !!");
        }
    }

    private static void ex7(Scanner scanner) throws IOException {
        Path path= Paths.get(scanner.nextLine());
        Scanner lineScanner = new Scanner(path);
        Scanner WordScanner = new Scanner(path);
        String liniaMesLLarga = null;
        String liniaMesCurta = null;
        int minWordCounter = 0;
        int maxWordCounter = 0;
        while (lineScanner.hasNextLine()){
            int wordCounter = 0;
            while(WordScanner.hasNext()){
                WordScanner.next();
                wordCounter++;
            }
            if (wordCounter>maxWordCounter){
                liniaMesLLarga = lineScanner.toString();
            }else if (wordCounter<minWordCounter){
                liniaMesCurta = lineScanner.toString();
            }
            lineScanner.nextLine();
        }
        System.out.printf("Linia més llarga: %s\n", liniaMesLLarga);
        System.out.printf("Linia més curta: %s\n", liniaMesCurta);
    }


    private static void ex6() throws FileNotFoundException {
        String fileName = "C:\\Users\\User\\Desktop\\enters.txt";
        Scanner scan = new Scanner(new File(fileName));
        int count = 0;
        int suma = 0;
        while(scan.hasNextInt()){
            int number = scan.nextInt();
            count ++;
            suma += number;
            System.out.println(number);
        }
        System.out.printf("Hi ha un total de: %d numeros que sumen %d",count, suma);
    }

    private static void ex5() throws FileNotFoundException {
        String fileName = "/home/sjo/Escriptori/fitxer";
        Scanner scan = new Scanner(new File(fileName));
        while(scan.hasNextLine()){
            String line = scan.nextLine();
            System.out.println(line);
        }
    }

    private static void ex4() throws IOException {
        String file = "/home/sjo/Escriptori/fitxer";
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            stream.forEach(System.out::println);
        }
    }

    private static void ex3(Scanner scanner) throws IOException {
        BufferedWriter bw;
        FileWriter fw;
        File file = new File("/home/sjo/Escriptori/fitxer2");
        fw = new FileWriter(file.getAbsoluteFile(), true);
        bw = new BufferedWriter(fw);
        String input = scanner.nextLine();
        while (!Objects.equals(input, "END")){
            bw.write(input);
            bw.newLine();
            input = scanner.nextLine();
        }
        System.out.println("informació agregada!");
        bw.close();
        fw.close();
    }


    private static void ex2(Scanner scanner) throws IOException {
        BufferedWriter fichero = new BufferedWriter
                (new FileWriter("/home/sjo/Escriptori/fitxer2"));
        System.out.println("Introdueix el text:(Ultima linea ha de ser: END)");
        String input = scanner.nextLine();
        while (!Objects.equals(input, "END")){
            fichero.write(input);
            fichero.newLine();
            input = scanner.nextLine();
        }
        fichero.close();
        System.out.println("FITXER DESAT CORRECTAMENT !!");
    }


    private static void ex1(Scanner scanner) {
        System.out.println("Introdueix el directori a analitzar: ");
        String path = scanner.next();
        String[] pathnames;
        File f = new File(path);
        pathnames = f.list();
        for (String pathname : pathnames) {
            System.out.println(pathname);
        }
    }
}
