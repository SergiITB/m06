package products2;

import java.util.ArrayList;

public class Product2 {
    private String name;
    private int price;
    private int stock;
    private String picture;
    private ArrayList<Category> categories;

    public Product2(String name, int price, int stock, String picture, ArrayList<Category> categories) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.picture = picture;
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }

    public String getPicture() {
        return picture;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Product2{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", picture='" + picture + '\'' +
                ", categories=" + categories +
                '}';
    }
}
