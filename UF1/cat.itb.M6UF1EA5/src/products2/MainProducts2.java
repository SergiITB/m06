package products2;

import com.google.gson.Gson;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainProducts2 {
    public static void main(String[] args) {
        System.out.print("---------MENU--------" +
                "\n5) Exercici 5 (Llegir fitxer \"products2.json\" i mostrar les dades)" +
                "\n6) Exercici 6 (Escriure 5 nous productes al fitxer \"products2.json\")" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 5:
                ex5();
                break;
            case 6:
                ex6();
                break;
        }
    }

    private static void ex6() {
        // Creo una instancia de GSON
        Gson gson = new Gson();

        // Creo una llista de productes
        List<Product2> product2List = new ArrayList<>();
        product2List.add(new Product2("chocolate", 10, 25,"chocolate.png", new ArrayList<>(Arrays.asList(new Category(5,"food"),new Category(6,"chocolate")))));
        product2List.add(new Product2("leche", 5, 35,"leche.png", new ArrayList<>(Arrays.asList(new Category(7,"food"),new Category(3,"leche")))));
        product2List.add(new Product2("ps4", 500, 10,"ps4.png", new ArrayList<>(Arrays.asList(new Category(2,"gaming"),new Category(8,"technology")))));
        product2List.add(new Product2("tablet", 100, 28,"tablet.png", new ArrayList<>(Arrays.asList(new Category(1,"office"),new Category(9,"technology")))));
        product2List.add(new Product2("table", 90, 65,"table.png", new ArrayList<>(Arrays.asList(new Category(10,"home"),new Category(11,"garden")))));

        // Muestro el resultado
        System.out.println("Elements afegits correctament");

        // Escribo el fichero
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("cat.itb.M6UF1EA5/products2.json", true))) {
                bw.write("\n");
            for (Product2 product: product2List) {
                String json = gson.toJson(product);
                bw.write(json+"\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(MainProducts2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void ex5() {
        // Creo una instancia de GSON
        Gson gson = new Gson();

        //Leo el fixero i imprimo los datos
        try (BufferedReader br = new BufferedReader(new FileReader("cat.itb.M6UF1EA5/products2.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                Product2 product2 = gson.fromJson(linea, Product2.class);
                System.out.println(product2);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
