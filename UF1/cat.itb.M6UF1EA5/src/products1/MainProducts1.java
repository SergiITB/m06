package products1;

import com.google.gson.Gson;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainProducts1 {
    public static void main(String[] args) {
        System.out.print("---------MENU--------" +
                "\n2) Exercici 2 (Llegir fitxer \"products1.json\" i mostrar les dades)" +
                "\n3) Exercici 3 (Escriure 5 nous productes al fitxer \"products1.json\")" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
        }
    }


    private static void ex3() {
        // Creo una instancia de GSON
        Gson gson = new Gson();

        // Creo una llista de productes
        List<Product1> product1List = new ArrayList<>();
        product1List.add(new Product1("chocolate", 10, 25,"chocolate.png", new ArrayList<>(Arrays.asList("food", "chocolate"))));
        product1List.add(new Product1("leche", 5, 35,"leche.png", new ArrayList<>(Arrays.asList("food", "leche"))));
        product1List.add(new Product1("ps4", 500, 10,"ps4.png", new ArrayList<>(Arrays.asList("gaming", "technology"))));
        product1List.add(new Product1("tablet", 100, 28,"tablet.png", new ArrayList<>(Arrays.asList("office", "technology"))));
        product1List.add(new Product1("table", 90, 65,"table.png", new ArrayList<>(Arrays.asList("home", "garden"))));

        // Muestro el resultado
        System.out.println("Elements afegits correctament");

        // Escribo el fichero
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("cat.itb.M6UF1EA5/products1.json", true))) {
            for (Product1 product: product1List) {
                String json = gson.toJson(product);
                bw.write(json+"\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(MainProducts1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private static void ex2() {
        // Creo una instancia de GSON
        Gson gson = new Gson();

        //Leo el fixero i imprimo los datos
        try (BufferedReader br = new BufferedReader(new FileReader("cat.itb.M6UF1EA5/products1.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                Product1 product1 = gson.fromJson(linea, Product1.class);
                System.out.println(product1);
            }

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
