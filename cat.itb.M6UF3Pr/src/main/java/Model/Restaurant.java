package Model;

import java.util.List;
import java.io.Serializable;

public class Restaurant implements Serializable {
	private Address address;
	private String borough;
	private String cuisine;
	private List<GradesItem> grades;
	private String name;
	private String restaurant_id;

	public Restaurant(Address address, String borough, String cuisine, List<GradesItem> grades, String name, String restaurant_id) {
		this.address = address;
		this.borough = borough;
		this.cuisine = cuisine;
		this.grades = grades;
		this.name = name;
		this.restaurant_id = restaurant_id;
	}

	public void setAddress(Address address){
		this.address = address;
	}

	public Address getAddress(){
		return address;
	}

	public void setBorough(String borough){
		this.borough = borough;
	}

	public String getBorough(){
		return borough;
	}

	public void setCuisine(String cuisine){
		this.cuisine = cuisine;
	}

	public String getCuisine(){
		return cuisine;
	}

	public void setGrades(List<GradesItem> grades){
		this.grades = grades;
	}

	public List<GradesItem> getGrades(){
		return grades;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRestaurantId(String restaurant_id){
		this.restaurant_id = restaurant_id;
	}

	public String getRestaurantId(){
		return restaurant_id;
	}

	@Override
 	public String toString(){
		return 
			"Restaurant{" + 
			"address = '" + address + '\'' + 
			",borough = '" + borough + '\'' + 
			",cuisine = '" + cuisine + '\'' + 
			",grades = '" + grades + '\'' + 
			",name = '" + name + '\'' + 
			",restaurant_id = '" + restaurant_id + '\'' +
			"}";
		}
}