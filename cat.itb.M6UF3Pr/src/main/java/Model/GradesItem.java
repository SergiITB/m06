package Model;

import java.io.Serializable;

public class GradesItem implements Serializable {
	private Date date;
	private String grade;
	private int score;

	public void setDate(Date date){
		this.date = date;
	}

	public Date getDate(){
		return date;
	}

	public void setGrade(String grade){
		this.grade = grade;
	}

	public String getGrade(){
		return grade;
	}

	public void setScore(int score){
		this.score = score;
	}

	public int getScore(){
		return score;
	}

	@Override
 	public String toString(){
		return 
			"GradesItem{" + 
			"date = '" + date + '\'' + 
			",grade = '" + grade + '\'' + 
			",score = '" + score + '\'' + 
			"}";
		}
}