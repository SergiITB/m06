package Model;

import java.io.Serializable;

public class Date implements Serializable {
	private long date;

	public void setDate(long date){
		this.date = date;
	}

	public long getDate(){
		return date;
	}

	@Override
 	public String toString(){
		return 
			"Date{" + 
			"$date = '" + date + '\'' + 
			"}";
		}
}