import Crud.RestaurantsCRUD;
import Model.Address;
import Model.Restaurant;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println(
                "---------MENU--------" +
                "\n--------PRACTICA 1--------"+
                "\n1) Exercici 1      8) Exercici 8" +
                "\n2) Exercici 2      9) Exercici 9" +
                "\n3) Exercici 3      10) Exercici 10" +
                "\n4) Exercici 4      11) Exercici 11" +
                "\n5) Exercici 5      12) Exercici 12" +
                "\n6) Exercici 6      13) Exercici 13" +
                "\n7) Exercici 7"+
                "\n--------PRACTICA 2--------"+
                "\n14) Exercici 1      17) Exercici 4" +
                "\n15) Exercici 2      18) Exercici 5" +
                "\n16) Exercici 3" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                RestaurantsCRUD.deleteCollection("itb","restaurants");
                break;
            case 2:
                RestaurantsCRUD.getRestaurantsFromFile("itb","restaurants","restaurants.json");
                break;
            case 3:
                Address address1 = new Address("3850",null,"Richmond Avenue","10312");
                Restaurant restaurant1  = new Restaurant(address1,"Staten Island","Pizza",null,"David Vaquer - El pizzero espagnol","99999999");

                Double[] coords2 = new Double[]{-73.9812198,40.7509706};
                Address address2 = new Address("24", List.of(coords2), "East 39 Street", "10016");
                Restaurant restaurant2  = new Restaurant(address2,"Manhattan","American",null,"Joan_Gomez's Hamburgers","99999991");

                RestaurantsCRUD.insertRestaurant("itb","restaurants",restaurant1);
                RestaurantsCRUD.insertRestaurant("itb","restaurants",restaurant2);
                break;
            case 4:
                RestaurantsCRUD.createIndex("itb","restaurants","restaurantId",true);
                break;
            case 5:
                RestaurantsCRUD.mostrarRestaurantWithBoroughAndCuisine();
                break;
            case 6:
                RestaurantsCRUD.mostrarRestaurantWithZipcode("10462");
                break;
            case 7:
                RestaurantsCRUD.mostrarRestaurantWithBarri();
                break;
            case 8:
                RestaurantsCRUD.mostrarRestaurantWithBuilding();
                break;
            case 9:
                RestaurantsCRUD.updateZipCode();
                break;
            case 10:
                RestaurantsCRUD.addCampStats();
                break;
            case 11:
                RestaurantsCRUD.updateCoordenades();
                break;
            case 12:
                RestaurantsCRUD.deleteRestaurantsByCuisine();
                break;
            case 13:
                RestaurantsCRUD.showNumGradesAndSumScoreByRestaurantID();
                break;

            //PRACTICA 2
            case 14:
                RestaurantsCRUD.showRestaurantsBorough();
                break;
            case 15:
                //NO FUNCIONA
                RestaurantsCRUD.showBoroughMinRestaurants();
                break;
            case 16:
                RestaurantsCRUD.numRestaurantsPerTipusCuina();
                break;
            case 17:
                RestaurantsCRUD.restaurantsMin5Grades();
                break;
            case 18:
                RestaurantsCRUD.showCuisinePerBorough();
                break;
        }
    }
}
