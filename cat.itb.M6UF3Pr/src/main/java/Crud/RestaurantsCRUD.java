package Crud;

import Connections.MongoClusterConnection;
import Model.Restaurant;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoWriteException;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Updates.*;
import static java.util.Arrays.asList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class RestaurantsCRUD {
    public static void deleteCollection(String database, String collection){
        MongoDatabase db = MongoClusterConnection.getDatabase(database);
        db.getCollection(collection).drop();
        System.out.println("Colecció eliminada!!");
        ListCollectionsIterable<Document> listCollections = db.listCollections();
        System.out.println("COL·LECCIONS EXISTENTS:");
        listCollections.forEach(System.out::println);
        MongoClusterConnection.closeConnection();
    }

    public static void getRestaurantsFromFile(String database, String collection, String file){
        String stringFile = "";

        try (BufferedReader br = new BufferedReader(new FileReader(RestaurantsCRUD.class.getClassLoader().getResource(file).getFile()))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Gson gson = new Gson();

        Restaurant[] restaurants = gson.fromJson(stringFile, Restaurant[].class);

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase(database);
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Restaurant> collectionBook = db.getCollection(collection,Restaurant.class);
        collectionBook.insertMany(asList(restaurants));
        MongoClusterConnection.closeConnection();
    }

    public static void insertRestaurant(String database, String collection, Restaurant restaurant){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase(database);
        db = db.withCodecRegistry(pojoCodecRegistry);
        try {
            MongoCollection<Restaurant> collectionRestaurant = db.getCollection(collection,Restaurant.class);
            collectionRestaurant.insertOne(restaurant);
        }catch (MongoWriteException dke){
            System.out.println("ERROR: Clau primaria duplicada !!");
        }
        MongoClusterConnection.closeConnection();
    }

    public static void createIndex(String database, String collection, String key, boolean unique){
        MongoDatabase myDB = MongoClusterConnection.getDatabase(database);
        MongoCollection<Document> coll = myDB.getCollection(collection);
        IndexOptions indexOptions = new IndexOptions().unique(unique);
        coll.createIndex(Indexes.ascending(key), indexOptions);
        System.out.printf("Index de clau unica: %s creat correctament!!\n",key);
        System.out.println("INDEXS COL·LECCIÓ "+collection);
        for (Document doc : coll.listIndexes()){
            System.out.println(doc);
        }
    }

    public static void mostrarRestaurantWithBoroughAndCuisine(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collectionRestaurant = db.getCollection("restaurants");
        FindIterable<Document> listRestaurant = collectionRestaurant.find(and(eq("borough","Manhattan"),eq("cuisine","Seafood")));
        System.out.println("************** - Mostra els restaurants on borough = Manhattan i cuisine = Seafood. **************");
        listRestaurant.forEach(System.out::println);
        MongoClusterConnection.closeConnection();
    }

    public static void mostrarRestaurantWithZipcode(String zipcode){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        FindIterable<Document> listRestaurants = db.getCollection("restaurants").find(eq("address.zipcode", zipcode))
                .projection(Projections.fields(Projections.include("name"),Projections.excludeId()));
        System.out.println("************** - Mostra els restaurants amb el zipCode passat per parametre. **************");
        listRestaurants.forEach( document -> System.out.println(document.toJson()));
        MongoClusterConnection.closeConnection();
    }

    public static void mostrarRestaurantWithBarri(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        FindIterable<Document> listRestaurants = db.getCollection("restaurants").find(and(eq("borough", "Queens"),not(eq("cuisine","American"))))
                .projection(Projections.fields(Projections.include("name"),Projections.excludeId()));
        System.out.println("************** - Mostra els restaurants que estiguin situats al barri de \"Queens\" però que el tipus de cuina no sigui\n" +
                "\"American\". **************");
        listRestaurants.forEach( document -> System.out.println(document.toJson()));
        MongoClusterConnection.closeConnection();
    }

    public static void mostrarRestaurantWithBuilding(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        FindIterable<Document> listRestaurants = db.getCollection("restaurants").find(eq("address.building", "3406"))
                .projection(Projections.fields(Projections.include("name","cuisine"),Projections.excludeId()));
        System.out.println("************** - Mostra el nom i el tipus de cuina del restaurant que al camp \"building\" tingui el valor\n" +
                "\"3406\". **************");
        listRestaurants.forEach( document -> System.out.println(document.toJson()));
        MongoClusterConnection.closeConnection();
    }

    public static void updateZipCode() {
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collectionRestaurant = db.getCollection("restaurants");
        UpdateResult updateResult = collectionRestaurant.updateMany(eq("address.street","Charles Street"), set("address.zipcode", "30033"));
        System.out.println("************** Actualitza el zipcode\" del carrer \"Charles Street\", el nou \"zipcode\" serà \"30033\". **************");
        System.out.println("Num. de registres actualitzats: "+updateResult.getModifiedCount());
    }

    public static void addCampStats(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collectionRestaurant = db.getCollection("restaurants");
        UpdateResult updateResult = collectionRestaurant.updateMany(eq("cuisine","Caribbean"), addToSet("stars", "*****"));
        System.out.println("************** Afegeixi el camp \"stars\" = \"*****\" a tots els Restaurants de cuisine Caribbean. **************");
        System.out.println("Num. de registres actualitzats: "+updateResult.getModifiedCount());
        FindIterable<Document> listRestaurants = db.getCollection("restaurants").find(eq("stars", "*****"))
                .projection(Projections.fields(Projections.include("name"),Projections.excludeId()));
        System.out.println("************** - Registres actualizats: **************");
        listRestaurants.forEach( document -> System.out.println(document.toJson()));
    }

    public static void updateCoordenades(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collectionRestaurant = db.getCollection("restaurants");
        Double[] coords = new Double[]{-73.996970,40.72532};
        collectionRestaurant.updateMany(eq("restaurantId","40368271"), set("address.coord", List.of(coords)));
        System.out.println("************** Actualitza les coordenades del restaurant amb \"restaurant_id\": \"40368271\". Les noves\n" +
                "coordenades són \"-73.996970, 40.72532\". **************");
        Document restaurantUpdated = db.getCollection("restaurants").find(eq("restaurantId", "40368271")).first();
        System.out.println("************** - Registre actualizat: **************");
        System.out.println(restaurantUpdated);
    }

    public static void deleteRestaurantsByCuisine() {
        System.out.println("\n************** - Elimina tots els restaurants on el cuisine = \"Delicatessen\". **************");
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");
        DeleteResult deleteResultCompany = collection.deleteMany(eq("cuisine","Delicatessen"));
        System.out.println("Num restaurants eliminats: "+deleteResultCompany.getDeletedCount());
        MongoClusterConnection.closeConnection();
    }

    public static void showNumGradesAndSumScoreByRestaurantID(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");

        Bson numGradesCollection = project(Projections.fields(Projections.excludeId(),
        Projections.include("name","restaurantId"),Projections.computed("numGrades",
                Projections.computed("$size", "$grades"))));

        Bson sumScoresCollection = project(Projections.fields(Projections.excludeId(),
                Projections.include("name","restaurantId"),Projections.computed("sumScores",
                        Projections.computed("$sum", "$grades.score"))));

        Document numGrades = collection.aggregate(asList(numGradesCollection,match(Filters.eq("restaurantId","40361521")))).first();
        Document sumScores = collection.aggregate(asList(sumScoresCollection,match(Filters.eq("restaurantId","40361521")))).first();

        System.out.printf("\nRestaurantId: %s, Nom Restarant: %s, Num Grades: %s, Suma Scores: %s",
                numGrades.get("restaurantId"), numGrades.get("name"), numGrades.get("numGrades"),sumScores.get("sumScores"));
    }

    public static void showRestaurantsBorough(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");

        System.out.println("\u001B[34mTOTAL RESTAURANTS PER BARRI:\u001B[0m");

        AggregateIterable<Document> result;
        result = collection.aggregate(List.of(group("$borough",sum("numRestaurants",1)),sort(descending("numRestaurants"))));
        result.forEach(System.out::println);
    }

    public static void showBoroughMinRestaurants(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");

        System.out.println("\u001B[34mBARRI AMB MENYS RESTAURANTS:\u001B[0m");

        AggregateIterable<Document> result;
        result = collection.aggregate(List.of(group("$borough"),min("min","$borough")));
        result.forEach(System.out::println);
//        Bson borderingCountriesCollection = project(Projections.fields(Projections.excludeId(),
//                Projections.include("name"),Projections.computed("restaurantsPerBarri",
//                        Projections.computed("$size", "$borders"))));
//
//        //Aggregates
//        int maxValue = collection.aggregate(asList(borderingCountriesCollection,
//                group(null, Accumulators.min("min", "$restaurantsPerBarri"))))
//                .first().getInteger("min");
//
//        Document maxNeighboredCountry = collection.aggregate(asList(borderingCountriesCollection,
//                match(Filters.eq("borderingCountries", maxValue)))).first();
    }

    public static void numRestaurantsPerTipusCuina(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");

        System.out.println("\u001B[34mNUM DE RESTAURANTS PER TIPUS DE CUINA:\u001B[0m");

        AggregateIterable<Document> result;
        result = collection.aggregate(List.of(group("$cuisine", sum("numRestaurants",1)),sort(descending("numRestaurants"))));
        result.forEach(System.out::println);
    }

    public static void restaurantsMin5Grades(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");

        System.out.println("\u001B[34mRESTAURANTS WITH 5 OR MORE GRADES:\u001B[0m");

        Bson fieldsIncluded = Projections.include("name");
        Bson projection = project(Projections.fields(
                Projections.excludeId(),
                fieldsIncluded,
                Projections.computed("gradesNum",
                        Projections.computed("$size","$grades"))));

        AggregateIterable<Document> result;
        result = collection.aggregate(List.of(projection,match(gte("gradesNum",5))));
        result.forEach(System.out::println);
    }

    public static void showCuisinePerBorough(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("restaurants");

        System.out.println("\u001B[34mNOMS DE TIPUS DE CUINA PER BARRI:\u001B[0m");

        AggregateIterable<Document> result;
        result = collection.aggregate(List.of(group("$borough",Accumulators.addToSet("tipusCuina","$cuisine"))));
        result.forEach(System.out::println);
    }
}
