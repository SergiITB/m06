package pojo;

import java.util.List;
import java.io.Serializable;

public class Country implements Serializable {
	private String name;
	private List<String> topLevelDomain;
	private String alpha2Code;
	private String alpha3Code;
	private List<String> callingCodes;
	private String capital;
	private List<String> altSpellings;
	private String region;
	private String subregion;
	private int population;
	private List<Double> latlng;
	private String demonym;
	private Double area;
	private Double gini;
	private List<String> timezones;
	private List<String> borders;
	private String nativeName;
	private String numericCode;
	private List<CurrenciesItem> currencies;
	private List<LanguagesItem> languages;
	private Translations translations;
	private String flag;
	private List<RegionalBlocsItem> regionalBlocs;
	private String cioc;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTopLevelDomain(List<String> topLevelDomain){
		this.topLevelDomain = topLevelDomain;
	}

	public List<String> getTopLevelDomain(){
		return topLevelDomain;
	}

	public void setAlpha2Code(String alpha2Code){
		this.alpha2Code = alpha2Code;
	}

	public String getAlpha2Code(){
		return alpha2Code;
	}

	public void setAlpha3Code(String alpha3Code){
		this.alpha3Code = alpha3Code;
	}

	public String getAlpha3Code(){
		return alpha3Code;
	}

	public void setCallingCodes(List<String> callingCodes){
		this.callingCodes = callingCodes;
	}

	public List<String> getCallingCodes(){
		return callingCodes;
	}

	public void setCapital(String capital){
		this.capital = capital;
	}

	public String getCapital(){
		return capital;
	}

	public void setAltSpellings(List<String> altSpellings){
		this.altSpellings = altSpellings;
	}

	public List<String> getAltSpellings(){
		return altSpellings;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setSubregion(String subregion){
		this.subregion = subregion;
	}

	public String getSubregion(){
		return subregion;
	}

	public void setPopulation(int population){
		this.population = population;
	}

	public int getPopulation(){
		return population;
	}

	public void setLatlng(List<Double> latlng){
		this.latlng = latlng;
	}

	public List<Double> getLatlng(){
		return latlng;
	}

	public void setDemonym(String demonym){
		this.demonym = demonym;
	}

	public String getDemonym(){
		return demonym;
	}

	public void setArea(Double area){
		this.area = area;
	}

	public Object getArea(){
		return area;
	}

	public void setGini(Double gini){
		this.gini = gini;
	}

	public Object getGini(){
		return gini;
	}

	public void setTimezones(List<String> timezones){
		this.timezones = timezones;
	}

	public List<String> getTimezones(){
		return timezones;
	}

	public void setBorders(List<String> borders){
		this.borders = borders;
	}

	public List<String> getBorders(){
		return borders;
	}

	public void setNativeName(String nativeName){
		this.nativeName = nativeName;
	}

	public String getNativeName(){
		return nativeName;
	}

	public void setNumericCode(String numericCode){
		this.numericCode = numericCode;
	}

	public String getNumericCode(){
		return numericCode;
	}

	public void setCurrencies(List<CurrenciesItem> currencies){
		this.currencies = currencies;
	}

	public List<CurrenciesItem> getCurrencies(){
		return currencies;
	}

	public void setLanguages(List<LanguagesItem> languages){
		this.languages = languages;
	}

	public List<LanguagesItem> getLanguages(){
		return languages;
	}

	public void setTranslations(Translations translations){
		this.translations = translations;
	}

	public Translations getTranslations(){
		return translations;
	}

	public void setFlag(String flag){
		this.flag = flag;
	}

	public String getFlag(){
		return flag;
	}

	public void setRegionalBlocs(List<RegionalBlocsItem> regionalBlocs){
		this.regionalBlocs = regionalBlocs;
	}

	public List<RegionalBlocsItem> getRegionalBlocs(){
		return regionalBlocs;
	}

	public void setCioc(String cioc){
		this.cioc = cioc;
	}

	public String getCioc(){
		return cioc;
	}

	@Override
 	public String toString(){
		return 
			"Country{" + 
			"name = '" + name + '\'' + 
			",topLevelDomain = '" + topLevelDomain + '\'' + 
			",alpha2Code = '" + alpha2Code + '\'' + 
			",alpha3Code = '" + alpha3Code + '\'' + 
			",callingCodes = '" + callingCodes + '\'' + 
			",capital = '" + capital + '\'' + 
			",altSpellings = '" + altSpellings + '\'' + 
			",region = '" + region + '\'' + 
			",subregion = '" + subregion + '\'' + 
			",population = '" + population + '\'' + 
			",latlng = '" + latlng + '\'' + 
			",demonym = '" + demonym + '\'' + 
			",area = '" + area + '\'' + 
			",gini = '" + gini + '\'' + 
			",timezones = '" + timezones + '\'' + 
			",borders = '" + borders + '\'' + 
			",nativeName = '" + nativeName + '\'' + 
			",numericCode = '" + numericCode + '\'' + 
			",currencies = '" + currencies + '\'' + 
			",languages = '" + languages + '\'' + 
			",translations = '" + translations + '\'' + 
			",flag = '" + flag + '\'' + 
			",regionalBlocs = '" + regionalBlocs + '\'' + 
			",cioc = '" + cioc + '\'' + 
			"}";
		}
}