package pojo;

import java.io.Serializable;

public class CurrenciesItem implements Serializable {
	private String code;
	private String name;
	private String symbol;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setSymbol(String symbol){
		this.symbol = symbol;
	}

	public String getSymbol(){
		return symbol;
	}

	@Override
 	public String toString(){
		return 
			"CurrenciesItem{" + 
			"code = '" + code + '\'' + 
			",name = '" + name + '\'' + 
			",symbol = '" + symbol + '\'' + 
			"}";
		}
}