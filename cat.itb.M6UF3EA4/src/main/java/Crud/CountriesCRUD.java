package Crud;

import Connexio.MongoClusterConnection;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoWriteException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.ListIndexesIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import pojo.Country;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Indexes.descending;
import static java.util.Arrays.asList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class CountriesCRUD {

    public static void numPaisosPerSubregio(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("countries");
        AggregateIterable<Document> grup = collection.aggregate(asList(unwind("$subregion"),
                group("$subregion", sum("country", 1))));
        System.out.println("\nEXERCICI 3 APARTAT B)");
        grup.forEach(rest -> System.out.println(rest.toJson()));
        MongoClusterConnection.closeConnection();
    }

    public static void paisMaxIdiomes(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("countries");
        Bson languagesCountriesCollection = project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("languagesCountry", Projections.computed("$size", "$languages"))));
        AggregateIterable<Document> maxNeighboredCountry = collection.aggregate(asList(languagesCountriesCollection,
                sort(Sorts.descending("languagesCountry")), limit(1)));
        System.out.println("\nEXERCICI 3 APARTAT C)");
        maxNeighboredCountry.forEach(System.out::println);
    }

    public static void insertCountry(String database, String collection, Document country){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase(database);
        db = db.withCodecRegistry(pojoCodecRegistry);
        try {
            MongoCollection<Document> collectionCountry = db.getCollection(collection);
            collectionCountry.insertOne(country);
        }catch (MongoWriteException dke){
            System.out.println("ERROR: Clau primaria duplicada !!");
        }
        MongoClusterConnection.closeConnection();
    }

    public static void createIndex(String database, String collection, String key, boolean unique){
        MongoDatabase myDB = MongoClusterConnection.getDatabase(database);
        MongoCollection<Document> coll = myDB.getCollection(collection);
        IndexOptions indexOptions = new IndexOptions().unique(unique);
        coll.createIndex(Indexes.ascending(key), indexOptions);
        System.out.printf("Index de clau unica: %s creat correctament!!\n",key);
    }

    public static void listIndexs(String database, String collection){
        MongoDatabase myDB = MongoClusterConnection.getDatabase(database);
        MongoCollection<Document> coll = myDB.getCollection(collection);
        ListIndexesIterable<Document> index = coll.listIndexes();
        System.out.printf("Indexs de clau unica de la col·lecció %s:\n",collection);
        index.forEach(document -> System.out.println(document.toJson()));
    }

    public static void dropCountries(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        long numDocuments = db.getCollection("countries").countDocuments();
        System.out.println("Num documents colecció countries: "+numDocuments);
        db.getCollection("countries").drop();
        System.out.println("Colecció eliminada!!");
        MongoClusterConnection.closeConnection();
    }

    public static void givenCountryCollection_whenNeighborsCalculated_thenMaxIsFifteenInChina4_4() {
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("countries");
        Bson borderingCountriesCollection = project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("borderingCountries",
                        Projections.computed("$size", "$borders"))));

        int maxValue = collection.aggregate(Arrays.asList(borderingCountriesCollection,
                group(null, Accumulators.max("max", "$borderingCountries"))))
                .first().getInteger("max");
        System.out.println("\nEXEMPLE: 4.4. project, group (with max), match");
        System.out.println("Max value: "+maxValue);

        Document maxNeighboredCountry = collection.aggregate(Arrays.asList(borderingCountriesCollection,
                match(Filters.eq("borderingCountries", maxValue)))).first();
        System.out.println(maxNeighboredCountry);
    }

    public static void givenCountryCollection_whenAreaSortedDescending_thenSuccess4_3() {
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("countries");
        collection.aggregate(Arrays.asList(
                sort(Sorts.descending("area")),
                limit(7),
                out("largest_seven"))).toCollection();
        MongoCollection<Document> largestSeven = myDB.getCollection("largest_seven");
        System.out.println("\nEXEMPLE: 4.3. sort, limit, and out");
        System.out.println(largestSeven);
        Document usa = largestSeven.find(Filters.eq("alpha3Code", "USA")).first();
        System.out.println(usa);
    }

    public static void givenCountryCollection_whenCountedRegionWise_thenMaxInAfrica4_2() {
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("countries");
        Document maxCountriedRegion = collection.aggregate(Arrays.asList(
                group("$region", Accumulators.sum("tally", 1)),
                sort(Sorts.descending("tally")))).first();
        System.out.println("\nEXEMPLE: 4.2. group (with sum) and sort");
        System.out.println(maxCountriedRegion);
    }

    public static void givenCountryCollection_whenEnglishSpeakingCountries4_1() {
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("countries");
        Document englishSpeakingCountries = collection.aggregate(Arrays.asList(
                match(Filters.eq("languages.name", "English")),
                count())).first();
        System.out.println("\nEXEMPLE: 4.1. match and count");
        System.out.println(englishSpeakingCountries);
    }

    public static void getCountriesFromFile(String database, String collection, String file){
        String stringFile = "";

        try (BufferedReader br = new BufferedReader(new FileReader(CountriesCRUD.class.getClassLoader().getResource(file).getFile()))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Gson gson = new Gson();

        Country[] countries = gson.fromJson(stringFile, Country[].class);

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase(database);
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Country> collectionBook = db.getCollection(collection,Country.class);
        collectionBook.insertMany(asList(countries));
        MongoClusterConnection.closeConnection();
    }

}
