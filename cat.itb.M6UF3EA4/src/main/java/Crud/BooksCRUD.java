package Crud;

import Connexio.MongoClusterConnection;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Indexes.descending;
import static java.util.Arrays.asList;

public class BooksCRUD {
    public static void llibresMin5Authors(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("books");
        AggregateIterable<Document> grup = collection.aggregate(asList(
                match(Filters.gt("authors.size",5)),sort(descending("authors"))));
        grup.forEach(rest -> System.out.println(rest.toJson()));
        MongoClusterConnection.closeConnection();
    }
}
