import Crud.BooksCRUD;
import Crud.CountriesCRUD;
import org.bson.Document;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("---------MENU--------" +
                "\n1) Exercici 1" +
                "\n2) Exercici 2 " +
                "\n3) Exercici 3" +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                CountriesCRUD.dropCountries();
                CountriesCRUD.getCountriesFromFile("itb","countries","countries.json");
                break;
            case 2:
                CountriesCRUD.createIndex("itb","countries","name",true);
                CountriesCRUD.listIndexs("itb","countries");
                Document country = new Document();
                country.append("name","Spain");
                CountriesCRUD.insertCountry("itb","countries",country);
                break;
            case 3:
                CountriesCRUD.givenCountryCollection_whenEnglishSpeakingCountries4_1();
                CountriesCRUD.givenCountryCollection_whenCountedRegionWise_thenMaxInAfrica4_2();
                CountriesCRUD.givenCountryCollection_whenAreaSortedDescending_thenSuccess4_3();
                CountriesCRUD.givenCountryCollection_whenNeighborsCalculated_thenMaxIsFifteenInChina4_4();
                CountriesCRUD.numPaisosPerSubregio();
                CountriesCRUD.paisMaxIdiomes();
//                NO FUNCIONA:
//                BooksCRUD.llibresMin5Authors();
                break;
        }
    }
}