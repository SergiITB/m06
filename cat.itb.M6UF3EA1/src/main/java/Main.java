import Connexio.ConnexioAtlas;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lt;
import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        MongoClient mongoClient = ConnexioAtlas.getConnexio();

        System.out.println("---------MENU--------" +
                "\n1) Exercici 1: Insertar 2 estudiants"+
                "\n2) Exercici 2: Mostrar dades dels estudiants."+
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                insertarStudents(mongoClient);
                break;
            case 2:
                mostrarDadesEstudiants(mongoClient);
                break;
        }
    }

    private static void mostrarDadesEstudiants(MongoClient mongoClient) {
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("itb");
        FindIterable<Document> listGrades;

        listGrades = sampleTrainingDB.getCollection("grades").find(new Document("group", "B"));
        System.out.println("************** Students del grup B **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));

        listGrades = sampleTrainingDB.getCollection("grades").find(eq("scores",
                Document.parse("{'type': 'exam', 'score': 100 }")));
        System.out.println("************** Students amb 100 al exam **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));

        listGrades = sampleTrainingDB.getCollection("grades").find(lt("scores",
                Document.parse("{'type': 'exam', 'score': 50 }")));
        System.out.println("************** Students amb menys de 50 al exam **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));

        listGrades = sampleTrainingDB.getCollection("grades").find(new Document("student_id", 123));
        System.out.println("************** Interessos del estudiant amb student_id = 123 **************");
        listGrades.forEach( document -> System.out.println(document.toJson()));
    }

    private static void insertarStudents(MongoClient mongoClient) {
        MongoDatabase sampleTrainingDB = mongoClient.getDatabase("itb");
        MongoCollection<Document> gradesCollection = sampleTrainingDB.getCollection("grades");

        Document student1 = new Document("_id", new ObjectId());
        student1.append("student_id", 111)
                .append("name","Sergi")
                .append("surname","Santos Díez")
                .append("class_id", "DAM")
                .append("group","B")
                .append("scores", asList(
                        new Document("type", "exam").append("score", 100),
                        new Document("type", "teamWork").append("score", 50)));
        Document student2 = new Document("_id", new ObjectId());
        student2.append("student_id", 123)
                .append("name","Carles")
                .append("surname","Dominguez Sanchez")
                .append("class_id", "Undefined")
                .append("group","B")
                .append("interests", asList("music","gym","code","electronics"));
        List<Document> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        gradesCollection.insertMany(students);
    }
}
