package Connexio;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class ConnexioAtlas {

    private static MongoClient mongoClient;

    public  static MongoClient getConnexio() {
        mongoClient = MongoClients.create("mongodb+srv://dbSergi:2020ITB143@cluster0.1ktcq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
        return mongoClient;
    }

    public static MongoDatabase getSampleTrainingDB(){
        return mongoClient.getDatabase("sample_training");
    }
}
