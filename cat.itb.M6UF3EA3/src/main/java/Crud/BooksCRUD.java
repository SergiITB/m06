package Crud;

import Connexio.MongoClusterConnection;
import Pojo.Book;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoWriteException;
import com.mongodb.client.*;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Updates.*;
import static java.util.Arrays.asList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class BooksCRUD {
    public static void dropBooks(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        long numDocuments = db.getCollection("books").countDocuments();
        System.out.println("Num documents colecció books: "+numDocuments);
        db.getCollection("books").drop();
        System.out.println("Colecció eliminada!!");
        MongoClusterConnection.closeConnection();
    }

    public static Book[] getBooksFromFile(String file){
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(BooksCRUD.class.getClassLoader().getResource(file).getFile()))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Gson gson = new Gson();
        return gson.fromJson(stringFile, Book[].class);
    }

    public static long insertObjects(Book[] books){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        collectionBook.insertMany(asList(books));
        long count = collectionBook.countDocuments();
        MongoClusterConnection.closeConnection();
        return count;
    }

    public static void mostrarCampLongDescription(){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        FindIterable<Book> listBook = collectionBook.find();
        System.out.println("************** Mostra únicament el camp longDescription de tots els llibres. **************");
        listBook.forEach(book -> System.out.println(book.getLongDescription()+"\n"));
        MongoClusterConnection.closeConnection();
    }

    public static void mostrarTitleAuthorsInAuthorsArray(String[] authorsName){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        FindIterable<Book> listBook = collectionBook.find(all("authors",authorsName));
        System.out.println("************** Mostra el title, authors dels llibres on han participat almenys els autors: \"Charlie Collins\" i \"Robi Sen\". **************");
        listBook.forEach(book -> System.out.println("TITLE: "+book.getTitle()+" AUTHORS: "+book.getAuthors()));
        MongoClusterConnection.closeConnection();
    }

    public static void mostrarInfoBookCategoriaInArrayNoAuthor(String[] categories,String author){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        FindIterable<Book> listBook = collectionBook.find(combine(all("categories",categories),ne("authors",author))).sort(ascending("title"));
        System.out.println("************** Mostra els llibres que tinguin dins el camp categories la categoria \"Java\" però descartem els que surti \"Vikram\n" +
                "Goyal\" com author. Ordena ascendentment per title. **************");
        listBook.forEach(System.out::println);
        MongoClusterConnection.closeConnection();
    }

    public static void mostrarLlibresInCategoriesOrderTitleLimit15(String[] categories,int limit){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        FindIterable<Book> listBook = collectionBook.find(combine(in("categories",categories))).sort(ascending("title")).limit(limit);
        System.out.println("************** Mostra els llibres que tinguin dins el camp categories la categoria \"Mobile\" o \"Java\". Ordena ascendentment\n" +
                "per title i limita que es recullin només els 15 primers. **************");
        listBook.forEach(System.out::println);
        MongoClusterConnection.closeConnection();
    }


    public static void mostarDadesAmbNomLlibre(String nomAutor){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        FindIterable<Book>  listBook = collectionBook.find(eq("authors",nomAutor));
        System.out.println("************** - Mostra el title i authors dels llibres de \"Danno Ferrin\". **************");
        listBook.forEach(book -> System.out.println("TITOL: "+book.getTitle()+"  AUTORS:"+book.getAuthors()));
        MongoClusterConnection.closeConnection();
    }

    public static void mostarDadesAmbPaginesEnIntervalICategoria(int minPages, int maxPages, String categoria){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
        FindIterable<Book>  listBook = collectionBook.find(combine(lte("pageCount",maxPages),gte("pageCount",minPages),eq("categories",categoria)));
        System.out.println("************** Mostra el title, authors i pageCount dels llibres que tinguin entre 300 i 350 pàgines (ambdues incloses) i\n" +
                "siguin de categoria \"Java\". **************");
        listBook.forEach(book -> System.out.println("TITLE: "+book.getTitle()+"  AUTORS:"+book.getAuthors()+ " PAGE COUNT:"+book.getPageCount()));
        MongoClusterConnection.closeConnection();
    }

    public static void mostarCategoriesDiferents(){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Document> collectionBook = db.getCollection("books");
        MongoCursor<String> categories = collectionBook.distinct("categories",String.class).iterator();
        System.out.println("************** Mostra les categories diferents. **************");
        while (categories.hasNext()){
            System.out.println(categories.next());
        }
        MongoClusterConnection.closeConnection();
    }

    public static void mostarAutorsDiferents(){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Document> collectionBook = db.getCollection("books");
        MongoCursor<String> autors = collectionBook.distinct("authors",String.class).iterator();
        System.out.println("************** Mostra els autors diferents. **************");
        while (autors.hasNext()){
            System.out.println(autors.next());
        }
        MongoClusterConnection.closeConnection();
    }

    public static long setAssesment(int value){
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Book> collection = myDB.getCollection("books",Book.class);
        System.out.println("************** Afegeix un camp anomenat \"Assessment\" a tots els llibres amb\n" +
                "categoria \"Internet\" i el status \"PUBLISH\". **************");
        UpdateResult updateResult = collection.updateMany(combine(eq("categories","Internet"),eq("status", "PUBLISH")), set("Assessment", value));
        return updateResult.getModifiedCount();
    }

    public static long addAuthor(String author,String title){
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Book> collection = myDB.getCollection("books",Book.class);
        System.out.println("************** Afegeix un camp anomenat \"Assessment\" a tots els llibres amb\n" +
                "categoria \"Internet\" i el status \"PUBLISH\". **************");
        UpdateResult updateResult = collection.updateMany(eq("title",title), addToSet("authors", author));
        return updateResult.getModifiedCount();
    }

    public static void createUniqueKeyIndex(String key){
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("books");
        IndexOptions indexOptions = new IndexOptions().unique(true);
        collection.createIndex(Indexes.ascending(key), indexOptions);
    }

    public static void showAllIndex(){
        MongoDatabase myDB = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = myDB.getCollection("books");
        ListIndexesIterable<Document> index = collection.listIndexes();
        index.forEach(document -> System.out.println(document.toJson()));
    }

    public static void insertDuplicatedBookError(){
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        db = db.withCodecRegistry(pojoCodecRegistry);
        try {
            MongoCollection<Book> collectionBook = db.getCollection("books",Book.class);
            Book book = new Book();
            book.setId(300);
            book.setTitle("Unlocking Android");
            book.setIsbn("1933988673");
            collectionBook.insertOne(book);
        }catch (MongoWriteException dke){
            System.out.println("ERROR: ISBN Duplicada !!");
        }
        MongoClusterConnection.closeConnection();
    }

    public static void eliminarLlibresPerAutor(String authorName) {
        System.out.println("\n************** - Elimina tots els llibres escrits per l'autor \"Kyle Baley\" . **************");
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Book> collection = db.getCollection("books",Book.class);
        DeleteResult deleteResultCompany = collection.deleteMany(eq("authors",authorName));
        System.out.println("Num llibres eliminats: "+deleteResultCompany.getDeletedCount());
        MongoClusterConnection.closeConnection();
    }

    public static void eliminarLlibrePerTitol(String title) {
        System.out.println("\n************** - Elimina el llibre amb títol \"Coffeehouse\". **************");
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Book> collection = db.getCollection("books",Book.class);
        DeleteResult deleteResultCompany = collection.deleteOne(eq("title",title));
        System.out.println("Num llibres eliminats: "+deleteResultCompany.getDeletedCount());
        MongoClusterConnection.closeConnection();
    }

    public static void eliminarCampStatusLlibres(String title) {
        System.out.println("\n************** - Elimina el camp status de tots els llibres. **************");
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Book> collection = db.getCollection("books",Book.class);
        final long modifiedCount = collection.updateMany(gte("_id",0), unset(title)).getMatchedCount();
        System.out.println("Registres actualitzats: "+ modifiedCount+"\n");
        MongoClusterConnection.closeConnection();
    }

    public static void numLlibresPerAutor(){
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("books");
        AggregateIterable<Document> grup = collection.aggregate(asList(unwind("$authors"),
                group("$authors", sum("numLlibres", 2)),
                sort(descending("title"))));
        grup.forEach(rest -> System.out.println(rest.toJson()));
        MongoClusterConnection.closeConnection();
    }
}
