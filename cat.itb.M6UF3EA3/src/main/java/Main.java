import Crud.BooksCRUD;
import Pojo.Book;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("---------MENU--------" +
                "\n1) Exercici 1" +
                "\n2) Exercici 2 " +
                "\n3) Exercici 3" +
                "\n4) Exercici 4 " +
                "\n5) Exercici 5 " +
                "\n6) Exercici 6 " +
                "\n\nINTRODUEIX L'EXERCICI A REALITZAR: ");
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                BooksCRUD.dropBooks();
                Book[] books = BooksCRUD.getBooksFromFile("books.json");
                for (Book book:books){System.out.println(book);}
                System.out.println("S'han insertat: "+BooksCRUD.insertObjects(books)+" books");
                break;
            case 2:
                BooksCRUD.mostrarCampLongDescription();
                BooksCRUD.mostarDadesAmbNomLlibre("Danno Ferrin");
                BooksCRUD.mostarDadesAmbPaginesEnIntervalICategoria(300,350,"Java");
                BooksCRUD.mostrarTitleAuthorsInAuthorsArray(new String[]{"Charlie Collins","Robi Sen"});
                BooksCRUD.mostrarInfoBookCategoriaInArrayNoAuthor(new String[]{"Java"},"Vikram Goyal");
                BooksCRUD.mostrarLlibresInCategoriesOrderTitleLimit15(new String[]{"Mobile","Java"},15);
                BooksCRUD.mostarCategoriesDiferents();
                BooksCRUD.mostarAutorsDiferents();
                break;
            case 3:
                long setAssesment = BooksCRUD.setAssesment(5000);
                System.out.println("Num. registres actualitzats: "+setAssesment);
                long addAuthor = BooksCRUD.addAuthor("Roger Whatch","Unlocking Android");
                System.out.println("Num. registres actualitzats: "+addAuthor);
                break;
            case 4:
                BooksCRUD.createUniqueKeyIndex("isbn");
                BooksCRUD.showAllIndex();
                BooksCRUD.insertDuplicatedBookError();
                break;
            case 5:
                BooksCRUD.eliminarLlibresPerAutor("Kyle Baley");
                BooksCRUD.eliminarLlibrePerTitol("Coffeehouse");
                BooksCRUD.eliminarCampStatusLlibres("status");
                break;
            case 6:
                BooksCRUD.numLlibresPerAutor();
                break;
        }
    }
}
